﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_tF95C9E01A913DD50575531C8305932628663D9E9;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t349E66EC5F09B881A8E52EE40A1AB9EC60E08E44;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.String>>
struct List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC;
// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D;
// System.Collections.Generic.List`1<System.String>
struct List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD;
// UnityEngine.Events.UnityEvent`1<UnityEngine.SpriteRenderer>
struct UnityEvent_1_t8ABE5544759145B8D7A09F1C54FFCB6907EDD56E;
// System.Collections.Generic.List`1<System.String>[]
struct List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// Selectable[]
struct SelectableU5BU5D_t6B1728DC4B5C4A0E251A489FA114488E47166D0A;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// UpdateSprite[]
struct UpdateSpriteU5BU5D_tA884ABBF823CA957C7F6D74CD175979A907EBE93;
// UnityEngine.Camera
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184;
// UnityEngine.Collider2D
struct Collider2D_t6A17BA7734600EF3F26588E9ED903617D5B8EB52;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.Collections.IEnumerator
struct IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// ScoreKeeper
struct ScoreKeeper_tB7132D9BFE1E78E0A894A9A29FD4B83B37262492;
// Selectable
struct Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745;
// Solitaire
struct Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240;
// UnityEngine.Sprite
struct Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1;
// UIButtons
struct UIButtons_t4B44681F20665393EEF37CC4F0D19073DE59048E;
// UpdateSprite
struct UpdateSprite_tDC00DA6185261B3C937D81EC2F99CDC5A9DEB83F;
// UserInput
struct UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD;
// Solitaire/<SolitaireDeal>d__28
struct U3CSolitaireDealU3Ed__28_t1551387CF05830C6119332636FEF237FE63DA94B;

IL2CPP_EXTERN_C RuntimeClass* IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CSolitaireDealU3Ed__28_t1551387CF05830C6119332636FEF237FE63DA94B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral09684B67A5909FD48E1F14A8AF8DDD483C620B10;
IL2CPP_EXTERN_C String_t* _stringLiteral0EBD646B60E1C3FCE0203770591ED3C3D63537DC;
IL2CPP_EXTERN_C String_t* _stringLiteral1690B3E0A7ABF26C7432995D1219914EE9822024;
IL2CPP_EXTERN_C String_t* _stringLiteral2064F80F811DB79A33C4E51C10221454E30C74AE;
IL2CPP_EXTERN_C String_t* _stringLiteral21ED4C7AF50D987589A9029FC0422151BE3A0FC2;
IL2CPP_EXTERN_C String_t* _stringLiteral25D74BC981E6316A5E3CAEEA0BAF3C438F5C94DA;
IL2CPP_EXTERN_C String_t* _stringLiteral2F7234099CCD07F9C0939ACCC13D7F7F6E95DBAD;
IL2CPP_EXTERN_C String_t* _stringLiteral3D714DD3E8E77A697EF557E85ED2B014A96328C5;
IL2CPP_EXTERN_C String_t* _stringLiteral410E5346BCA8EE150FFD507311DD85789F2E171E;
IL2CPP_EXTERN_C String_t* _stringLiteral49A7EA21ECB328D154FA2262BB41626D795F4D90;
IL2CPP_EXTERN_C String_t* _stringLiteral4DE7F5C690036C169C4E241BCAC36102AEE31873;
IL2CPP_EXTERN_C String_t* _stringLiteral6C64D1112BBF8B2A09059130C37F2ED556ECEC47;
IL2CPP_EXTERN_C String_t* _stringLiteral7402F76B2D6078A455F77607AF6E7350B6DE019E;
IL2CPP_EXTERN_C String_t* _stringLiteral7454695E25D304C65D0C1333D8008E862569CAE9;
IL2CPP_EXTERN_C String_t* _stringLiteral994AF4767E315DC605A2C546002D5A8F201D5D16;
IL2CPP_EXTERN_C String_t* _stringLiteral9DAB8995495FBD15B7CFC865BE4B64C25615CCBF;
IL2CPP_EXTERN_C String_t* _stringLiteralA019FB7F17AA36A9743C530E1F11D5613B8B1158;
IL2CPP_EXTERN_C String_t* _stringLiteralAA4DA1A93F07E2E4B09F8D5E2C45905E26A45E79;
IL2CPP_EXTERN_C String_t* _stringLiteralB157F89A1C7FC50EFF8E1244B8DB0FF3A13F5118;
IL2CPP_EXTERN_C String_t* _stringLiteralB16CF3324CA15FF0851B0F99DD86AC638C3E0CAE;
IL2CPP_EXTERN_C String_t* _stringLiteralBF86C9E9E7FE0EF09A2EAE8066CDC31F859254CC;
IL2CPP_EXTERN_C String_t* _stringLiteralC92E1C3CC8E3494BBF77478799B4D0624331268E;
IL2CPP_EXTERN_C String_t* _stringLiteralCC3B66F53DF5E0F136EE01571B02058391AD2CA1;
IL2CPP_EXTERN_C String_t* _stringLiteralD14EAC0D0010CD8B317D8E85A95D5E259525B9AC;
IL2CPP_EXTERN_C String_t* _stringLiteralE0EF57155BBB9B198D7E7295CFF8A209DE3D0008;
IL2CPP_EXTERN_C String_t* _stringLiteralE3F21DCF0EA9BEEE61C939C25E335B1DB4C9EF9E;
IL2CPP_EXTERN_C String_t* _stringLiteralE6D0E4273F680D4F1714F70306071EAF7F0CCDE1;
IL2CPP_EXTERN_C String_t* _stringLiteralE9B577CAE7605B964C63995C672D6F87AF3CAF63;
IL2CPP_EXTERN_C String_t* _stringLiteralEF420ABFDDBDA7B9EE665D85EF62E4A437554003;
IL2CPP_EXTERN_C String_t* _stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m8600576CD1B20D252F660EBD14F4BC39065A47B3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Last_TisString_t_m43259774830432D2A85E650A290C92CFB006C36A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mCFF42E67D10B26708DF415B8410EB546FC1611B5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m966C074B573F7DC85DADF3000EE50742A42CA71D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Contains_m359254483BE42CAD4DCA8FBAFB87473FB4CF00E1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveAt_m031D3A21689276A872FCA7566C8F2F79F9581F0D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_mD9E8CFB6777A99046B3C0195F7343FE771A2E99D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m420EE34051D5B5A5E9EEA1DDF7817068C641CB8E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mB63183A9151F4345A9DD444A7CBE0D6E03F77C7C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m21AEC50E791371101DC22ABCF96A2E46800811F8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m7C02F7C6C6FF4FCF6B9C8DD17961EED6BE51B3FD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectOfType_TisSolitaire_t43D61047DE323D7D7559F84668F499D389CC1240_m7620A569538EA0B1B8B0F52835C0A76FCB017FF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectOfType_TisUserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7_mB6FCD395BC8C50FE972E391CE11CC4DD9646B6D0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectsOfType_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m83681642548ACBB0992224E09175AB18E0B65DBE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectsOfType_TisUpdateSprite_tDC00DA6185261B3C937D81EC2F99CDC5A9DEB83F_m2E109204B762B96BDA11780FA3909A47C8027B22_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_m887DD9E8844786738857F4A68C25C7AADC55FFA0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Solitaire_Shuffle_TisString_t_m785218983FE9B697535D0FFD54C0F1A878C0497A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CSolitaireDealU3Ed__28_System_Collections_IEnumerator_Reset_m2B23C74A6DCDF1A9C0C25A801E8D3D2C4FC090BC_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E;
struct GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF;
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
struct SelectableU5BU5D_t6B1728DC4B5C4A0E251A489FA114488E47166D0A;
struct SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
struct UpdateSpriteU5BU5D_tA884ABBF823CA957C7F6D74CD175979A907EBE93;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tBB65183F1134474D09FF49B95625D25472B9BA8B 
{
};

// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.String>>
struct List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.String>
struct List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___s_emptyArray_5;
};
struct Il2CppArrayBounds;

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
};

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	RuntimeObject* ____current_3;
};

// System.Collections.Generic.List`1/Enumerator<System.String>
struct Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	String_t* ____current_3;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17 
{
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;
};

struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17_StaticFields
{
	// System.Byte[] System.Char::s_categoryForLatin1
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___s_categoryForLatin1_3;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;
};

struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion_4;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector_9;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	float ___m_Seconds_0;
};

// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};

struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.RaycastHit2D
struct RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA 
{
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;
};

// Solitaire/<SolitaireDeal>d__28
struct U3CSolitaireDealU3Ed__28_t1551387CF05830C6119332636FEF237FE63DA94B  : public RuntimeObject
{
	// System.Int32 Solitaire/<SolitaireDeal>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Solitaire/<SolitaireDeal>d__28::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// Solitaire Solitaire/<SolitaireDeal>d__28::<>4__this
	Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* ___U3CU3E4__this_2;
	// System.Int32 Solitaire/<SolitaireDeal>d__28::<i>5__2
	int32_t ___U3CiU3E5__2_3;
	// System.Single Solitaire/<SolitaireDeal>d__28::<yOffset>5__3
	float ___U3CyOffsetU3E5__3_4;
	// System.Single Solitaire/<SolitaireDeal>d__28::<zOffset>5__4
	float ___U3CzOffsetU3E5__4_5;
	// System.Collections.Generic.List`1/Enumerator<System.String> Solitaire/<SolitaireDeal>d__28::<>7__wrap4
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 ___U3CU3E7__wrap4_6;
	// System.String Solitaire/<SolitaireDeal>d__28::<card>5__6
	String_t* ___U3CcardU3E5__6_7;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Sprite
struct Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// UnityEngine.Renderer
struct Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Camera
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184_StaticFields
{
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD* ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD* ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD* ___onPostRender_6;
};

// UnityEngine.Collider2D
struct Collider2D_t6A17BA7734600EF3F26588E9ED903617D5B8EB52  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B  : public Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF
{
	// UnityEngine.Events.UnityEvent`1<UnityEngine.SpriteRenderer> UnityEngine.SpriteRenderer::m_SpriteChangeEvent
	UnityEvent_1_t8ABE5544759145B8D7A09F1C54FFCB6907EDD56E* ___m_SpriteChangeEvent_4;
};

// ScoreKeeper
struct ScoreKeeper_tB7132D9BFE1E78E0A894A9A29FD4B83B37262492  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Selectable[] ScoreKeeper::topStacks
	SelectableU5BU5D_t6B1728DC4B5C4A0E251A489FA114488E47166D0A* ___topStacks_4;
	// UnityEngine.GameObject ScoreKeeper::highScorePanel
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___highScorePanel_5;
};

// Selectable
struct Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Boolean Selectable::top
	bool ___top_4;
	// System.String Selectable::suit
	String_t* ___suit_5;
	// System.Int32 Selectable::value
	int32_t ___value_6;
	// System.Int32 Selectable::row
	int32_t ___row_7;
	// System.Boolean Selectable::faceUp
	bool ___faceUp_8;
	// System.Boolean Selectable::inDeckPile
	bool ___inDeckPile_9;
	// System.String Selectable::valueString
	String_t* ___valueString_10;
};

// Solitaire
struct Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Sprite[] Solitaire::cardFaces
	SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* ___cardFaces_4;
	// UnityEngine.GameObject Solitaire::cardPrefab
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___cardPrefab_5;
	// UnityEngine.GameObject Solitaire::deckButton
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___deckButton_6;
	// UnityEngine.GameObject[] Solitaire::bottomPos
	GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* ___bottomPos_7;
	// UnityEngine.GameObject[] Solitaire::topPos
	GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* ___topPos_8;
	// System.Collections.Generic.List`1<System.String>[] Solitaire::bottoms
	List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* ___bottoms_11;
	// System.Collections.Generic.List`1<System.String>[] Solitaire::tops
	List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* ___tops_12;
	// System.Collections.Generic.List`1<System.String> Solitaire::tripsOnDisplay
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___tripsOnDisplay_13;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.String>> Solitaire::deckTrips
	List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC* ___deckTrips_14;
	// System.Collections.Generic.List`1<System.String> Solitaire::bottom0
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___bottom0_15;
	// System.Collections.Generic.List`1<System.String> Solitaire::bottom1
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___bottom1_16;
	// System.Collections.Generic.List`1<System.String> Solitaire::bottom2
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___bottom2_17;
	// System.Collections.Generic.List`1<System.String> Solitaire::bottom3
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___bottom3_18;
	// System.Collections.Generic.List`1<System.String> Solitaire::bottom4
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___bottom4_19;
	// System.Collections.Generic.List`1<System.String> Solitaire::bottom5
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___bottom5_20;
	// System.Collections.Generic.List`1<System.String> Solitaire::bottom6
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___bottom6_21;
	// System.Collections.Generic.List`1<System.String> Solitaire::deck
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___deck_22;
	// System.Collections.Generic.List`1<System.String> Solitaire::discardPile
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___discardPile_23;
	// System.Int32 Solitaire::deckLocation
	int32_t ___deckLocation_24;
	// System.Int32 Solitaire::trips
	int32_t ___trips_25;
	// System.Int32 Solitaire::tripsRemainder
	int32_t ___tripsRemainder_26;
};

struct Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_StaticFields
{
	// System.String[] Solitaire::suits
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___suits_9;
	// System.String[] Solitaire::values
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___values_10;
};

// UIButtons
struct UIButtons_t4B44681F20665393EEF37CC4F0D19073DE59048E  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.GameObject UIButtons::highScorePanel
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___highScorePanel_4;
};

// UpdateSprite
struct UpdateSprite_tDC00DA6185261B3C937D81EC2F99CDC5A9DEB83F  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Sprite UpdateSprite::cardFace
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___cardFace_4;
	// UnityEngine.Sprite UpdateSprite::cardBack
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___cardBack_5;
	// UnityEngine.SpriteRenderer UpdateSprite::spriteRenderer
	SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* ___spriteRenderer_6;
	// Selectable UpdateSprite::selectable
	Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* ___selectable_7;
	// Solitaire UpdateSprite::solitaire
	Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* ___solitaire_8;
	// UserInput UpdateSprite::userInput
	UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* ___userInput_9;
};

// UserInput
struct UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.GameObject UserInput::slot1
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___slot1_4;
	// Solitaire UserInput::solitaire
	Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* ___solitaire_5;
	// System.Single UserInput::timer
	float ___timer_6;
	// System.Single UserInput::doubleClickTime
	float ___doubleClickTime_7;
	// System.Int32 UserInput::clickCount
	int32_t ___clickCount_8;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Selectable[]
struct SelectableU5BU5D_t6B1728DC4B5C4A0E251A489FA114488E47166D0A  : public RuntimeArray
{
	ALIGN_FIELD (8) Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* m_Items[1];

	inline Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Collections.Generic.List`1<System.String>[]
struct List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E  : public RuntimeArray
{
	ALIGN_FIELD (8) List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* m_Items[1];

	inline List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF  : public RuntimeArray
{
	ALIGN_FIELD (8) GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* m_Items[1];

	inline GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t76FEDD663AB33C991A9C9A23129337651094216F** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t76FEDD663AB33C991A9C9A23129337651094216F** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UpdateSprite[]
struct UpdateSpriteU5BU5D_tA884ABBF823CA957C7F6D74CD175979A907EBE93  : public RuntimeArray
{
	ALIGN_FIELD (8) UpdateSprite_tDC00DA6185261B3C937D81EC2F99CDC5A9DEB83F* m_Items[1];

	inline UpdateSprite_tDC00DA6185261B3C937D81EC2F99CDC5A9DEB83F* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline UpdateSprite_tDC00DA6185261B3C937D81EC2F99CDC5A9DEB83F** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, UpdateSprite_tDC00DA6185261B3C937D81EC2F99CDC5A9DEB83F* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline UpdateSprite_tDC00DA6185261B3C937D81EC2F99CDC5A9DEB83F* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline UpdateSprite_tDC00DA6185261B3C937D81EC2F99CDC5A9DEB83F** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, UpdateSprite_tDC00DA6185261B3C937D81EC2F99CDC5A9DEB83F* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Sprite[]
struct SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B  : public RuntimeArray
{
	ALIGN_FIELD (8) Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* m_Items[1];

	inline Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void Solitaire::Shuffle<System.Object>(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Solitaire_Shuffle_TisRuntimeObject_mC36D74A84161D8DDDC0E3404D1BF9C0F1157A6D1_gshared (Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* __this, List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* ___list0, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A List_1_GetEnumerator_mD8294A7FA2BEB1929487127D476F8EC1CDC23BFC_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mD9DC3E3C3697830A4823047AB29A77DBBB5ED419_gshared (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mE921CC8F29FBBDE7CC3209A0ED0D921D58D00BCB_gshared (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) ;
// TSource System.Linq.Enumerable::Last<System.Object>(System.Collections.Generic.IEnumerable`1<TSource>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Last_TisRuntimeObject_mCAB07698079CD05A16F547890122EC092ACC25E0_gshared (RuntimeObject* ___source0, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_RemoveAt_m54F62297ADEE4D4FDA697F49ED807BF901201B54_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, int32_t ___index0, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* List_1_get_Item_m33561245D64798C2AB07584C0EC4F240E4839A38_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, int32_t ___index0, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Remove_m4DFA48F4CEB9169601E75FC28517C5C06EFA5AD7_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) ;
// T UnityEngine.Object::Instantiate<System.Object>(T,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Object_Instantiate_TisRuntimeObject_mE974DCECE6BCBB030F70F1618B707F587B99BDB4_gshared (RuntimeObject* ___original0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position1, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation2, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___parent3, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Contains_m4C9139C2A6B23E9343D3F87807B32C6E2CFE660D_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) ;
// T[] UnityEngine.Object::FindObjectsOfType<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* Object_FindObjectsOfType_TisRuntimeObject_m1E6D851F6A46D646E0554A94729E9AAE79B0E87A_gshared (const RuntimeMethod* method) ;
// T UnityEngine.Object::FindObjectOfType<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Object_FindObjectOfType_TisRuntimeObject_m9990A7304DF02BA1ED160587D1C2F6DAE89BB343_gshared (const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;

// System.Boolean ScoreKeeper::HasWon()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ScoreKeeper_HasWon_mCA808C163BB4AFE1B799C0C9A14FDD8CEF69C450 (ScoreKeeper_tB7132D9BFE1E78E0A894A9A29FD4B83B37262492* __this, const RuntimeMethod* method) ;
// System.Void ScoreKeeper::Win()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScoreKeeper_Win_mA7910B2F38648B795B67DE595622EFB542DCE565 (ScoreKeeper_tB7132D9BFE1E78E0A894A9A29FD4B83B37262492* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, bool ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_print_mED815C779E369787B3E9646A6DE96FBC2944BF0B (RuntimeObject* ___message0, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Component::CompareTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Component_CompareTag_mE6F8897E84F12DF12D302FFC4D58204D51096FC5 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, String_t* ___tag0, const RuntimeMethod* method) ;
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* __this, const RuntimeMethod* method) ;
// System.Char System.String::get_Chars(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3 (String_t* __this, int32_t ___index0, const RuntimeMethod* method) ;
// System.String System.Char::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Char_ToString_m2A308731F9577C06AF3C0901234E2EAC8327410C (Il2CppChar* __this, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method) ;
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) ;
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0 (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method) ;
// System.Void Solitaire::PlayCards()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Solitaire_PlayCards_m72D3A39228F204F5FDCAFA486A64D92459F2AFB0 (Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.String>::Clear()
inline void List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_inline (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*, const RuntimeMethod*))List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline)(__this, method);
}
// System.Collections.Generic.List`1<System.String> Solitaire::GenerateDeck()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* Solitaire_GenerateDeck_mE430BEF13A823C578E35C2137BFA3A660BE22446 (const RuntimeMethod* method) ;
// System.Void Solitaire::Shuffle<System.String>(System.Collections.Generic.List`1<T>)
inline void Solitaire_Shuffle_TisString_t_m785218983FE9B697535D0FFD54C0F1A878C0497A (Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* __this, List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___list0, const RuntimeMethod* method)
{
	((  void (*) (Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240*, List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*, const RuntimeMethod*))Solitaire_Shuffle_TisRuntimeObject_mC36D74A84161D8DDDC0E3404D1BF9C0F1157A6D1_gshared)(__this, ___list0, method);
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.String>::GetEnumerator()
inline Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 (*) (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*, const RuntimeMethod*))List_1_GetEnumerator_mD8294A7FA2BEB1929487127D476F8EC1CDC23BFC_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::Dispose()
inline void Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7 (Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1*, const RuntimeMethod*))Enumerator_Dispose_mD9DC3E3C3697830A4823047AB29A77DBBB5ED419_gshared)(__this, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.String>::get_Current()
inline String_t* Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline (Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1* __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1*, const RuntimeMethod*))Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.String>::MoveNext()
inline bool Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED (Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1*, const RuntimeMethod*))Enumerator_MoveNext_mE921CC8F29FBBDE7CC3209A0ED0D921D58D00BCB_gshared)(__this, method);
}
// System.Void Solitaire::SolitaireSort()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Solitaire_SolitaireSort_m6403D2229F335EE10D5629B2B319726858989CE5 (Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* __this, const RuntimeMethod* method) ;
// System.Collections.IEnumerator Solitaire::SolitaireDeal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Solitaire_SolitaireDeal_mF910B118C91BF7DE326EA9489F36A28F003FEF61 (Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* __this, const RuntimeMethod* method) ;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812 (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, RuntimeObject* ___routine0, const RuntimeMethod* method) ;
// System.Void Solitaire::SortDeckIntoTrips()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Solitaire_SortDeckIntoTrips_m6A43E9681644C41D6B7743B9B0B45A5066490ADB (Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::Add(T)
inline void List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* __this, String_t* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*, String_t*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___item0, method);
}
// System.Void Solitaire/<SolitaireDeal>d__28::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSolitaireDealU3Ed__28__ctor_m4853FC99373F6E44BEF3F1AEF9AB8AF90B6829B9 (U3CSolitaireDealU3Ed__28_t1551387CF05830C6119332636FEF237FE63DA94B* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// TSource System.Linq.Enumerable::Last<System.String>(System.Collections.Generic.IEnumerable`1<TSource>)
inline String_t* Enumerable_Last_TisString_t_m43259774830432D2A85E650A290C92CFB006C36A (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  String_t* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_Last_TisRuntimeObject_mCAB07698079CD05A16F547890122EC092ACC25E0_gshared)(___source0, method);
}
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
inline int32_t List_1_get_Count_mB63183A9151F4345A9DD444A7CBE0D6E03F77C7C_inline (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*, const RuntimeMethod*))List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_m031D3A21689276A872FCA7566C8F2F79F9581F0D (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* __this, int32_t ___index0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*, int32_t, const RuntimeMethod*))List_1_RemoveAt_m54F62297ADEE4D4FDA697F49ED807BF901201B54_gshared)(__this, ___index0, method);
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.String>>::Clear()
inline void List_1_Clear_m966C074B573F7DC85DADF3000EE50742A42CA71D_inline (List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC*, const RuntimeMethod*))List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline)(__this, method);
}
// T System.Collections.Generic.List`1<System.String>::get_Item(System.Int32)
inline String_t* List_1_get_Item_m21AEC50E791371101DC22ABCF96A2E46800811F8 (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  String_t* (*) (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*, int32_t, const RuntimeMethod*))List_1_get_Item_m33561245D64798C2AB07584C0EC4F240E4839A38_gshared)(__this, ___index0, method);
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.String>>::Add(T)
inline void List_1_Add_mCFF42E67D10B26708DF415B8410EB546FC1611B5_inline (List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC* __this, List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC*, List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___item0, method);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Transform_GetEnumerator_mA7E1C882ACA0C33E284711CD09971DEA3FFEF404 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1<System.String>::Remove(T)
inline bool List_1_Remove_mD9E8CFB6777A99046B3C0195F7343FE771A2E99D (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* __this, String_t* ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*, String_t*, const RuntimeMethod*))List_1_Remove_m4DFA48F4CEB9169601E75FC28517C5C06EFA5AD7_gshared)(__this, ___item0, method);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___obj0, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.String>>::get_Item(System.Int32)
inline List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* List_1_get_Item_m7C02F7C6C6FF4FCF6B9C8DD17961EED6BE51B3FD (List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC* __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* (*) (List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC*, int32_t, const RuntimeMethod*))List_1_get_Item_m33561245D64798C2AB07584C0EC4F240E4839A38_gshared)(__this, ___index0, method);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_get_identity_mB9CAEEB21BC81352CBF32DB9664BFC06FA7EA27B_inline (const RuntimeMethod* method) ;
// T UnityEngine.Object::Instantiate<UnityEngine.GameObject>(T,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
inline GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_m887DD9E8844786738857F4A68C25C7AADC55FFA0 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___original0, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position1, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation2, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___parent3, const RuntimeMethod* method)
{
	return ((  GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1*, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mE974DCECE6BCBB030F70F1618B707F587B99BDB4_gshared)(___original0, ___position1, ___rotation2, ___parent3, method);
}
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_mC79E6DC8FFD72479C90F0C4CC7F42A0FEAF5AE47 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* __this, String_t* ___value0, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<Selectable>()
inline Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Void Solitaire::RestackTopDeck()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Solitaire_RestackTopDeck_m70300CC871C5AC743DB5C8A7234995430030C466 (Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.String>>::.ctor()
inline void List_1__ctor_m420EE34051D5B5A5E9EEA1DDF7817068C641CB8E (List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void Solitaire/<SolitaireDeal>d__28::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSolitaireDealU3Ed__28_U3CU3Em__Finally1_mE37C5D2D2EEBDE3FF25159019FB5691FE02237AA (U3CSolitaireDealU3Ed__28_t1551387CF05830C6119332636FEF237FE63DA94B* __this, const RuntimeMethod* method) ;
// System.Void Solitaire/<SolitaireDeal>d__28::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSolitaireDealU3Ed__28_System_IDisposable_Dispose_m35614D5BEBE733B6F300892713EADCEA91F7D394 (U3CSolitaireDealU3Ed__28_t1551387CF05830C6119332636FEF237FE63DA94B* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* __this, float ___seconds0, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1<System.String>::Contains(T)
inline bool List_1_Contains_m359254483BE42CAD4DCA8FBAFB87473FB4CF00E1 (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* __this, String_t* ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*, String_t*, const RuntimeMethod*))List_1_Contains_m4C9139C2A6B23E9343D3F87807B32C6E2CFE660D_gshared)(__this, ___item0, method);
}
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* __this, const RuntimeMethod* method) ;
// System.Void UIButtons::ResetScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIButtons_ResetScene_m12CD70C4CF320019CA5D036840D9E0A2F3CCC433 (UIButtons_t4B44681F20665393EEF37CC4F0D19073DE59048E* __this, const RuntimeMethod* method) ;
// T[] UnityEngine.Object::FindObjectsOfType<UpdateSprite>()
inline UpdateSpriteU5BU5D_tA884ABBF823CA957C7F6D74CD175979A907EBE93* Object_FindObjectsOfType_TisUpdateSprite_tDC00DA6185261B3C937D81EC2F99CDC5A9DEB83F_m2E109204B762B96BDA11780FA3909A47C8027B22 (const RuntimeMethod* method)
{
	return ((  UpdateSpriteU5BU5D_tA884ABBF823CA957C7F6D74CD175979A907EBE93* (*) (const RuntimeMethod*))Object_FindObjectsOfType_TisRuntimeObject_m1E6D851F6A46D646E0554A94729E9AAE79B0E87A_gshared)(method);
}
// System.Void UIButtons::ClearTopValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIButtons_ClearTopValues_m22554521111B98CD12C46FDF3EBFB171DF3A41A6 (UIButtons_t4B44681F20665393EEF37CC4F0D19073DE59048E* __this, const RuntimeMethod* method) ;
// T UnityEngine.Object::FindObjectOfType<Solitaire>()
inline Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* Object_FindObjectOfType_TisSolitaire_t43D61047DE323D7D7559F84668F499D389CC1240_m7620A569538EA0B1B8B0F52835C0A76FCB017FF2 (const RuntimeMethod* method)
{
	return ((  Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* (*) (const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m9990A7304DF02BA1ED160587D1C2F6DAE89BB343_gshared)(method);
}
// T[] UnityEngine.Object::FindObjectsOfType<Selectable>()
inline SelectableU5BU5D_t6B1728DC4B5C4A0E251A489FA114488E47166D0A* Object_FindObjectsOfType_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m83681642548ACBB0992224E09175AB18E0B65DBE (const RuntimeMethod* method)
{
	return ((  SelectableU5BU5D_t6B1728DC4B5C4A0E251A489FA114488E47166D0A* (*) (const RuntimeMethod*))Object_FindObjectsOfType_TisRuntimeObject_m1E6D851F6A46D646E0554A94729E9AAE79B0E87A_gshared)(method);
}
// T UnityEngine.Object::FindObjectOfType<UserInput>()
inline UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* Object_FindObjectOfType_TisUserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7_mB6FCD395BC8C50FE972E391CE11CC4DD9646B6D0 (const RuntimeMethod* method)
{
	return ((  UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* (*) (const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m9990A7304DF02BA1ED160587D1C2F6DAE89BB343_gshared)(method);
}
// T UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// T UnityEngine.Component::GetComponent<Selectable>()
inline Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* Component_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m8600576CD1B20D252F660EBD14F4BC39065A47B3 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer_set_sprite_m7B176E33955108C60CAE21DFC153A0FAC674CB53 (SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* __this, Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___value0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___exists0, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_yellow()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline (const RuntimeMethod* method) ;
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer_set_color_mB0EEC2845A0347E296C01C831F967731D2804546 (SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___value0, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_white()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_white_m28BB6E19F27D4EE6858D3021A44F62BC74E20C43_inline (const RuntimeMethod* method) ;
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D (const RuntimeMethod* method) ;
// System.Void UserInput::GetMouseClick()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInput_GetMouseClick_m25267C57BDB8466106B107405074860BAD41E21C (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetMouseButtonDown_m33522C56A54C402FE6DED802DD7E53435C27A5DE (int32_t ___button0, const RuntimeMethod* method) ;
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* Camera_get_main_mF222B707D3BF8CC9C7544609EFC71CFB62E81D43 (const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Input_get_mousePosition_m2414B43222ED0C5FAB960D393964189AFD21EEAD (const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Camera_ScreenToWorldPoint_m5EA3148F070985EC72127AAC3448D8D6ABE6E7E5 (Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position0, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Implicit_m8F73B300CB4E6F9B4EB5FB6130363D76CEAA230B_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___v0, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline (const RuntimeMethod* method) ;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA Physics2D_Raycast_m8D056DA6FCBB18FD6F64BA247C16E050B0DBBF18 (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___origin0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___direction1, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.RaycastHit2D::op_Implicit(UnityEngine.RaycastHit2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool RaycastHit2D_op_Implicit_m768ECEE43BC378B51CB2304A9D547E7683CC3EE5 (RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA ___hit0, const RuntimeMethod* method) ;
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Collider2D_t6A17BA7734600EF3F26588E9ED903617D5B8EB52* RaycastHit2D_get_collider_mB56DFCD16B708852EEBDBB490BC8665DBF7487FD (RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA* __this, const RuntimeMethod* method) ;
// System.Void UserInput::Deck()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInput_Deck_m81436BDC023279A2DF8FC3C7B07C18DEB6BAA7A4 (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, const RuntimeMethod* method) ;
// System.Void UserInput::Card(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInput_Card_m61BB20A7E54456DBE7881A622A3FCE4E7A1C001F (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___selected0, const RuntimeMethod* method) ;
// System.Void UserInput::Top(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInput_Top_m6D128BE13556EC650D464A52115774A60646862E (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___selected0, const RuntimeMethod* method) ;
// System.Void UserInput::Bottom(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInput_Bottom_m52CFBCBDEC44D99AB54CAB9C14B986085AF41C69 (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___selected0, const RuntimeMethod* method) ;
// System.Void Solitaire::DealFromDeck()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Solitaire_DealFromDeck_m34AC6775F731EFFCF36FED448B595069B90709AB (Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* __this, const RuntimeMethod* method) ;
// System.Boolean UserInput::Blocked(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UserInput_Blocked_m4830DFF904DB15168334ACD2991538AF1F7ECD18 (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___selected0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___x0, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___y1, const RuntimeMethod* method) ;
// System.Boolean UserInput::DoubleClick()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UserInput_DoubleClick_m5CE515EA8503B13815F9515D4899936F36935CF9 (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, const RuntimeMethod* method) ;
// System.Void UserInput::AutoStack(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInput_AutoStack_mE4DC3420E73DE955AE6987264F0DFD9FD8861B8B (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___selected0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___x0, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___y1, const RuntimeMethod* method) ;
// System.Boolean UserInput::Stackable(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UserInput_Stackable_m8F68A76920F4753B849A7FE5C7655711C0BD4BD2 (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___selected0, const RuntimeMethod* method) ;
// System.Void UserInput::Stack(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInput_Stack_m072D819EC1B4D6C4A15DDCCF3F121CE90E4695B7 (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___selected0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.GameObject::CompareTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_CompareTag_m6378BE50D009A93D46036F74CC3F7E2ECB0636E5 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, String_t* ___tag0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_parent_m9BD5E563B539DD5BEC342736B03F97B38A243234 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___value0, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method) ;
// System.Boolean UserInput::HasNoChildren(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UserInput_HasNoChildren_m4A22D7989817199809D0235DD74CB0A1CD5A9FA9 (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___card0, const RuntimeMethod* method) ;
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5 (int32_t* __this, const RuntimeMethod* method) ;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51 (String_t* ___name0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___x0, float ___y1, const RuntimeMethod* method) ;
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Clear_m48B57EC27CADC3463CA98A33373D557DA587FF1B (RuntimeArray* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ScoreKeeper::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScoreKeeper_Start_m111876C645A6D668BC752821007BE6877002C6A3 (ScoreKeeper_tB7132D9BFE1E78E0A894A9A29FD4B83B37262492* __this, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Void ScoreKeeper::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScoreKeeper_Update_m31E4B2F16670B50AD481128A788BF2ED3B607179 (ScoreKeeper_tB7132D9BFE1E78E0A894A9A29FD4B83B37262492* __this, const RuntimeMethod* method) 
{
	{
		// if (HasWon())
		bool L_0;
		L_0 = ScoreKeeper_HasWon_mCA808C163BB4AFE1B799C0C9A14FDD8CEF69C450(__this, NULL);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		// Win();
		ScoreKeeper_Win_mA7910B2F38648B795B67DE595622EFB542DCE565(__this, NULL);
	}

IL_000e:
	{
		// }
		return;
	}
}
// System.Boolean ScoreKeeper::HasWon()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ScoreKeeper_HasWon_mCA808C163BB4AFE1B799C0C9A14FDD8CEF69C450 (ScoreKeeper_tB7132D9BFE1E78E0A894A9A29FD4B83B37262492* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	SelectableU5BU5D_t6B1728DC4B5C4A0E251A489FA114488E47166D0A* V_1 = NULL;
	int32_t V_2 = 0;
	Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* V_3 = NULL;
	{
		// int i = 0;
		V_0 = 0;
		// foreach (Selectable topstack in topStacks)
		SelectableU5BU5D_t6B1728DC4B5C4A0E251A489FA114488E47166D0A* L_0 = __this->___topStacks_4;
		V_1 = L_0;
		V_2 = 0;
		goto IL_001e;
	}

IL_000d:
	{
		// foreach (Selectable topstack in topStacks)
		SelectableU5BU5D_t6B1728DC4B5C4A0E251A489FA114488E47166D0A* L_1 = V_1;
		int32_t L_2 = V_2;
		int32_t L_3 = L_2;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_4 = (L_1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_3));
		V_3 = L_4;
		// i += topstack.value;
		int32_t L_5 = V_0;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_6 = V_3;
		int32_t L_7 = L_6->___value_6;
		V_0 = ((int32_t)il2cpp_codegen_add(L_5, L_7));
		int32_t L_8 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_8, 1));
	}

IL_001e:
	{
		// foreach (Selectable topstack in topStacks)
		int32_t L_9 = V_2;
		SelectableU5BU5D_t6B1728DC4B5C4A0E251A489FA114488E47166D0A* L_10 = V_1;
		if ((((int32_t)L_9) < ((int32_t)((int32_t)(((RuntimeArray*)L_10)->max_length)))))
		{
			goto IL_000d;
		}
	}
	{
		// if (i >= 52)
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) < ((int32_t)((int32_t)52))))
		{
			goto IL_002b;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_002b:
	{
		// return false;
		return (bool)0;
	}
}
// System.Void ScoreKeeper::Win()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScoreKeeper_Win_mA7910B2F38648B795B67DE595622EFB542DCE565 (ScoreKeeper_tB7132D9BFE1E78E0A894A9A29FD4B83B37262492* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE6D0E4273F680D4F1714F70306071EAF7F0CCDE1);
		s_Il2CppMethodInitialized = true;
	}
	{
		// highScorePanel.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___highScorePanel_5;
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_0, (bool)1, NULL);
		// print("You have won!");
		MonoBehaviour_print_mED815C779E369787B3E9646A6DE96FBC2944BF0B(_stringLiteralE6D0E4273F680D4F1714F70306071EAF7F0CCDE1, NULL);
		// }
		return;
	}
}
// System.Void ScoreKeeper::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScoreKeeper__ctor_m2ACACD5BEC5B2FB90A3D014C60E3726F789EE357 (ScoreKeeper_tB7132D9BFE1E78E0A894A9A29FD4B83B37262492* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Selectable::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Selectable_Start_mBA7C139FC1AC9F6F234E222761AB29EAA295B957 (Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0EBD646B60E1C3FCE0203770591ED3C3D63537DC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2064F80F811DB79A33C4E51C10221454E30C74AE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral21ED4C7AF50D987589A9029FC0422151BE3A0FC2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral25D74BC981E6316A5E3CAEEA0BAF3C438F5C94DA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2F7234099CCD07F9C0939ACCC13D7F7F6E95DBAD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3D714DD3E8E77A697EF557E85ED2B014A96328C5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral49A7EA21ECB328D154FA2262BB41626D795F4D90);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7402F76B2D6078A455F77607AF6E7350B6DE019E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7454695E25D304C65D0C1333D8008E862569CAE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAA4DA1A93F07E2E4B09F8D5E2C45905E26A45E79);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB16CF3324CA15FF0851B0F99DD86AC638C3E0CAE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3F21DCF0EA9BEEE61C939C25E335B1DB4C9EF9E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEF420ABFDDBDA7B9EE665D85EF62E4A437554003);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppChar V_0 = 0x0;
	int32_t V_1 = 0;
	Il2CppChar V_2 = 0x0;
	{
		// if (CompareTag("Card"))
		bool L_0;
		L_0 = Component_CompareTag_mE6F8897E84F12DF12D302FFC4D58204D51096FC5(__this, _stringLiteralE3F21DCF0EA9BEEE61C939C25E335B1DB4C9EF9E, NULL);
		if (!L_0)
		{
			goto IL_01be;
		}
	}
	{
		// suit = transform.name[0].ToString();
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_1;
		L_1 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		String_t* L_2;
		L_2 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_1, NULL);
		Il2CppChar L_3;
		L_3 = String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3(L_2, 0, NULL);
		V_0 = L_3;
		String_t* L_4;
		L_4 = Char_ToString_m2A308731F9577C06AF3C0901234E2EAC8327410C((&V_0), NULL);
		__this->___suit_5 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___suit_5), (void*)L_4);
		// for (int i = 1; i < transform.name.Length; i++)
		V_1 = 1;
		goto IL_0061;
	}

IL_0033:
	{
		// char c = transform.name[i];
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_5;
		L_5 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		String_t* L_6;
		L_6 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_5, NULL);
		int32_t L_7 = V_1;
		Il2CppChar L_8;
		L_8 = String_get_Chars_mC49DF0CD2D3BE7BE97B3AD9C995BE3094F8E36D3(L_6, L_7, NULL);
		V_2 = L_8;
		// valueString = valueString + c.ToString();
		String_t* L_9 = __this->___valueString_10;
		String_t* L_10;
		L_10 = Char_ToString_m2A308731F9577C06AF3C0901234E2EAC8327410C((&V_2), NULL);
		String_t* L_11;
		L_11 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(L_9, L_10, NULL);
		__this->___valueString_10 = L_11;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___valueString_10), (void*)L_11);
		// for (int i = 1; i < transform.name.Length; i++)
		int32_t L_12 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_12, 1));
	}

IL_0061:
	{
		// for (int i = 1; i < transform.name.Length; i++)
		int32_t L_13 = V_1;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_14;
		L_14 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		String_t* L_15;
		L_15 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_14, NULL);
		int32_t L_16;
		L_16 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_15, NULL);
		if ((((int32_t)L_13) < ((int32_t)L_16)))
		{
			goto IL_0033;
		}
	}
	{
		// if (valueString == "A")
		String_t* L_17 = __this->___valueString_10;
		bool L_18;
		L_18 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_17, _stringLiteralEF420ABFDDBDA7B9EE665D85EF62E4A437554003, NULL);
		if (!L_18)
		{
			goto IL_008d;
		}
	}
	{
		// value = 1;
		__this->___value_6 = 1;
	}

IL_008d:
	{
		// if (valueString == "2")
		String_t* L_19 = __this->___valueString_10;
		bool L_20;
		L_20 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_19, _stringLiteral21ED4C7AF50D987589A9029FC0422151BE3A0FC2, NULL);
		if (!L_20)
		{
			goto IL_00a6;
		}
	}
	{
		// value = 2;
		__this->___value_6 = 2;
	}

IL_00a6:
	{
		// if (valueString == "3")
		String_t* L_21 = __this->___valueString_10;
		bool L_22;
		L_22 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_21, _stringLiteral2064F80F811DB79A33C4E51C10221454E30C74AE, NULL);
		if (!L_22)
		{
			goto IL_00bf;
		}
	}
	{
		// value = 3;
		__this->___value_6 = 3;
	}

IL_00bf:
	{
		// if (valueString == "4")
		String_t* L_23 = __this->___valueString_10;
		bool L_24;
		L_24 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_23, _stringLiteral7454695E25D304C65D0C1333D8008E862569CAE9, NULL);
		if (!L_24)
		{
			goto IL_00d8;
		}
	}
	{
		// value = 4;
		__this->___value_6 = 4;
	}

IL_00d8:
	{
		// if (valueString == "5")
		String_t* L_25 = __this->___valueString_10;
		bool L_26;
		L_26 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_25, _stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE, NULL);
		if (!L_26)
		{
			goto IL_00f1;
		}
	}
	{
		// value = 5;
		__this->___value_6 = 5;
	}

IL_00f1:
	{
		// if (valueString == "6")
		String_t* L_27 = __this->___valueString_10;
		bool L_28;
		L_28 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_27, _stringLiteral2F7234099CCD07F9C0939ACCC13D7F7F6E95DBAD, NULL);
		if (!L_28)
		{
			goto IL_010a;
		}
	}
	{
		// value = 6;
		__this->___value_6 = 6;
	}

IL_010a:
	{
		// if (valueString == "7")
		String_t* L_29 = __this->___valueString_10;
		bool L_30;
		L_30 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_29, _stringLiteral7402F76B2D6078A455F77607AF6E7350B6DE019E, NULL);
		if (!L_30)
		{
			goto IL_0123;
		}
	}
	{
		// value = 7;
		__this->___value_6 = 7;
	}

IL_0123:
	{
		// if (valueString == "8")
		String_t* L_31 = __this->___valueString_10;
		bool L_32;
		L_32 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_31, _stringLiteralB16CF3324CA15FF0851B0F99DD86AC638C3E0CAE, NULL);
		if (!L_32)
		{
			goto IL_013c;
		}
	}
	{
		// value = 8;
		__this->___value_6 = 8;
	}

IL_013c:
	{
		// if (valueString == "9")
		String_t* L_33 = __this->___valueString_10;
		bool L_34;
		L_34 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_33, _stringLiteral25D74BC981E6316A5E3CAEEA0BAF3C438F5C94DA, NULL);
		if (!L_34)
		{
			goto IL_0156;
		}
	}
	{
		// value = 9;
		__this->___value_6 = ((int32_t)9);
	}

IL_0156:
	{
		// if (valueString == "10")
		String_t* L_35 = __this->___valueString_10;
		bool L_36;
		L_36 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_35, _stringLiteralAA4DA1A93F07E2E4B09F8D5E2C45905E26A45E79, NULL);
		if (!L_36)
		{
			goto IL_0170;
		}
	}
	{
		// value = 10;
		__this->___value_6 = ((int32_t)10);
	}

IL_0170:
	{
		// if (valueString == "J")
		String_t* L_37 = __this->___valueString_10;
		bool L_38;
		L_38 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_37, _stringLiteral0EBD646B60E1C3FCE0203770591ED3C3D63537DC, NULL);
		if (!L_38)
		{
			goto IL_018a;
		}
	}
	{
		// value = 11;
		__this->___value_6 = ((int32_t)11);
	}

IL_018a:
	{
		// if (valueString == "Q")
		String_t* L_39 = __this->___valueString_10;
		bool L_40;
		L_40 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_39, _stringLiteral49A7EA21ECB328D154FA2262BB41626D795F4D90, NULL);
		if (!L_40)
		{
			goto IL_01a4;
		}
	}
	{
		// value = 12;
		__this->___value_6 = ((int32_t)12);
	}

IL_01a4:
	{
		// if (valueString == "K")
		String_t* L_41 = __this->___valueString_10;
		bool L_42;
		L_42 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_41, _stringLiteral3D714DD3E8E77A697EF557E85ED2B014A96328C5, NULL);
		if (!L_42)
		{
			goto IL_01be;
		}
	}
	{
		// value = 13;
		__this->___value_6 = ((int32_t)13);
	}

IL_01be:
	{
		// }
		return;
	}
}
// System.Void Selectable::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Selectable_Update_m24375E446EFA2E30F944F428F81DEA5B7A2D457C (Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* __this, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Void Selectable::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Selectable__ctor_mD3D03CF7FC80D1BEBFF53ED8BB01A4A1E16257D1 (Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Solitaire::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Solitaire_Start_m75E2A2392463F6D4123EA09D14AD4313140ED0ED (Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// bottoms = new List<string>[] { bottom0, bottom1, bottom2, bottom3, bottom4, bottom5, bottom6 };
		List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* L_0 = (List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E*)(List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E*)SZArrayNew(List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E_il2cpp_TypeInfo_var, (uint32_t)7);
		List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* L_1 = L_0;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_2 = __this->___bottom0_15;
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)L_2);
		List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* L_3 = L_1;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_4 = __this->___bottom1_16;
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)L_4);
		List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* L_5 = L_3;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_6 = __this->___bottom2_17;
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)L_6);
		List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* L_7 = L_5;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_8 = __this->___bottom3_18;
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)L_8);
		List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* L_9 = L_7;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_10 = __this->___bottom4_19;
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)L_10);
		List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* L_11 = L_9;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_12 = __this->___bottom5_20;
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(5), (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)L_12);
		List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* L_13 = L_11;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_14 = __this->___bottom6_21;
		ArrayElementTypeCheck (L_13, L_14);
		(L_13)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(6), (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)L_14);
		__this->___bottoms_11 = L_13;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___bottoms_11), (void*)L_13);
		// PlayCards();
		Solitaire_PlayCards_m72D3A39228F204F5FDCAFA486A64D92459F2AFB0(__this, NULL);
		// }
		return;
	}
}
// System.Void Solitaire::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Solitaire_Update_m41FC684A6F34B3F92DDD2F003ACCA59B1B8235EE (Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* __this, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Void Solitaire::PlayCards()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Solitaire_PlayCards_m72D3A39228F204F5FDCAFA486A64D92459F2AFB0 (Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Solitaire_Shuffle_TisString_t_m785218983FE9B697535D0FFD54C0F1A878C0497A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* V_0 = NULL;
	int32_t V_1 = 0;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// foreach (List<string> list in bottoms)
		List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* L_0 = __this->___bottoms_11;
		V_0 = L_0;
		V_1 = 0;
		goto IL_0017;
	}

IL_000b:
	{
		// foreach (List<string> list in bottoms)
		List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* L_1 = V_0;
		int32_t L_2 = V_1;
		int32_t L_3 = L_2;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_4 = (L_1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_3));
		// list.Clear();
		List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_inline(L_4, List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_RuntimeMethod_var);
		int32_t L_5 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_5, 1));
	}

IL_0017:
	{
		// foreach (List<string> list in bottoms)
		int32_t L_6 = V_1;
		List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* L_7 = V_0;
		if ((((int32_t)L_6) < ((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length)))))
		{
			goto IL_000b;
		}
	}
	{
		// deck = GenerateDeck();
		il2cpp_codegen_runtime_class_init_inline(Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_il2cpp_TypeInfo_var);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_8;
		L_8 = Solitaire_GenerateDeck_mE430BEF13A823C578E35C2137BFA3A660BE22446(NULL);
		__this->___deck_22 = L_8;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___deck_22), (void*)L_8);
		// Shuffle(deck);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_9 = __this->___deck_22;
		Solitaire_Shuffle_TisString_t_m785218983FE9B697535D0FFD54C0F1A878C0497A(__this, L_9, Solitaire_Shuffle_TisString_t_m785218983FE9B697535D0FFD54C0F1A878C0497A_RuntimeMethod_var);
		// foreach (string card in deck)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_10 = __this->___deck_22;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_11;
		L_11 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_10, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_2 = L_11;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0059:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_2), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_004e_1;
			}

IL_0042_1:
			{
				// foreach (string card in deck)
				String_t* L_12;
				L_12 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_2), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				// print(card);
				MonoBehaviour_print_mED815C779E369787B3E9646A6DE96FBC2944BF0B(L_12, NULL);
			}

IL_004e_1:
			{
				// foreach (string card in deck)
				bool L_13;
				L_13 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_2), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_13)
				{
					goto IL_0042_1;
				}
			}
			{
				goto IL_0067;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0067:
	{
		// SolitaireSort();
		Solitaire_SolitaireSort_m6403D2229F335EE10D5629B2B319726858989CE5(__this, NULL);
		// StartCoroutine(SolitaireDeal());
		RuntimeObject* L_14;
		L_14 = Solitaire_SolitaireDeal_mF910B118C91BF7DE326EA9489F36A28F003FEF61(__this, NULL);
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_15;
		L_15 = MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812(__this, L_14, NULL);
		// SortDeckIntoTrips();
		Solitaire_SortDeckIntoTrips_m6A43E9681644C41D6B7743B9B0B45A5066490ADB(__this, NULL);
		// }
		return;
	}
}
// System.Collections.Generic.List`1<System.String> Solitaire::GenerateDeck()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* Solitaire_GenerateDeck_mE430BEF13A823C578E35C2137BFA3A660BE22446 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_0 = NULL;
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* V_4 = NULL;
	int32_t V_5 = 0;
	String_t* V_6 = NULL;
	{
		// List<string> newDeck = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_0 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_0, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_0 = L_0;
		// foreach (string s in suits)
		il2cpp_codegen_runtime_class_init_inline(Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_il2cpp_TypeInfo_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_1 = ((Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_StaticFields*)il2cpp_codegen_static_fields_for(Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_il2cpp_TypeInfo_var))->___suits_9;
		V_1 = L_1;
		V_2 = 0;
		goto IL_0047;
	}

IL_0010:
	{
		// foreach (string s in suits)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_2 = V_1;
		int32_t L_3 = V_2;
		int32_t L_4 = L_3;
		String_t* L_5 = (L_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_4));
		V_3 = L_5;
		// foreach (string v in values)
		il2cpp_codegen_runtime_class_init_inline(Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_il2cpp_TypeInfo_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_6 = ((Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_StaticFields*)il2cpp_codegen_static_fields_for(Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_il2cpp_TypeInfo_var))->___values_10;
		V_4 = L_6;
		V_5 = 0;
		goto IL_003b;
	}

IL_0020:
	{
		// foreach (string v in values)
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_7 = V_4;
		int32_t L_8 = V_5;
		int32_t L_9 = L_8;
		String_t* L_10 = (L_7)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_9));
		V_6 = L_10;
		// newDeck.Add(s + v);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_11 = V_0;
		String_t* L_12 = V_3;
		String_t* L_13 = V_6;
		String_t* L_14;
		L_14 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(L_12, L_13, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_11, L_14, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		int32_t L_15 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add(L_15, 1));
	}

IL_003b:
	{
		// foreach (string v in values)
		int32_t L_16 = V_5;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_17 = V_4;
		if ((((int32_t)L_16) < ((int32_t)((int32_t)(((RuntimeArray*)L_17)->max_length)))))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_18 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_18, 1));
	}

IL_0047:
	{
		// foreach (string s in suits)
		int32_t L_19 = V_2;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_20 = V_1;
		if ((((int32_t)L_19) < ((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length)))))
		{
			goto IL_0010;
		}
	}
	{
		// return newDeck;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_21 = V_0;
		return L_21;
	}
}
// System.Collections.IEnumerator Solitaire::SolitaireDeal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Solitaire_SolitaireDeal_mF910B118C91BF7DE326EA9489F36A28F003FEF61 (Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSolitaireDealU3Ed__28_t1551387CF05830C6119332636FEF237FE63DA94B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CSolitaireDealU3Ed__28_t1551387CF05830C6119332636FEF237FE63DA94B* L_0 = (U3CSolitaireDealU3Ed__28_t1551387CF05830C6119332636FEF237FE63DA94B*)il2cpp_codegen_object_new(U3CSolitaireDealU3Ed__28_t1551387CF05830C6119332636FEF237FE63DA94B_il2cpp_TypeInfo_var);
		U3CSolitaireDealU3Ed__28__ctor_m4853FC99373F6E44BEF3F1AEF9AB8AF90B6829B9(L_0, 0, NULL);
		U3CSolitaireDealU3Ed__28_t1551387CF05830C6119332636FEF237FE63DA94B* L_1 = L_0;
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void Solitaire::SolitaireSort()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Solitaire_SolitaireSort_m6403D2229F335EE10D5629B2B319726858989CE5 (Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Last_TisString_t_m43259774830432D2A85E650A290C92CFB006C36A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_RemoveAt_m031D3A21689276A872FCA7566C8F2F79F9581F0D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mB63183A9151F4345A9DD444A7CBE0D6E03F77C7C_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// for (int i = 0; i < 7; i++)
		V_0 = 0;
		goto IL_0044;
	}

IL_0004:
	{
		// for (int j = i; j < 7; j++)
		int32_t L_0 = V_0;
		V_1 = L_0;
		goto IL_003c;
	}

IL_0008:
	{
		// bottoms[j].Add(deck.Last<string>());
		List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* L_1 = __this->___bottoms_11;
		int32_t L_2 = V_1;
		int32_t L_3 = L_2;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_4 = (L_1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_3));
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_5 = __this->___deck_22;
		String_t* L_6;
		L_6 = Enumerable_Last_TisString_t_m43259774830432D2A85E650A290C92CFB006C36A(L_5, Enumerable_Last_TisString_t_m43259774830432D2A85E650A290C92CFB006C36A_RuntimeMethod_var);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_4, L_6, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// deck.RemoveAt(deck.Count - 1);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_7 = __this->___deck_22;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_8 = __this->___deck_22;
		int32_t L_9;
		L_9 = List_1_get_Count_mB63183A9151F4345A9DD444A7CBE0D6E03F77C7C_inline(L_8, List_1_get_Count_mB63183A9151F4345A9DD444A7CBE0D6E03F77C7C_RuntimeMethod_var);
		List_1_RemoveAt_m031D3A21689276A872FCA7566C8F2F79F9581F0D(L_7, ((int32_t)il2cpp_codegen_subtract(L_9, 1)), List_1_RemoveAt_m031D3A21689276A872FCA7566C8F2F79F9581F0D_RuntimeMethod_var);
		// for (int j = i; j < 7; j++)
		int32_t L_10 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_10, 1));
	}

IL_003c:
	{
		// for (int j = i; j < 7; j++)
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)7)))
		{
			goto IL_0008;
		}
	}
	{
		// for (int i = 0; i < 7; i++)
		int32_t L_12 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_12, 1));
	}

IL_0044:
	{
		// for (int i = 0; i < 7; i++)
		int32_t L_13 = V_0;
		if ((((int32_t)L_13) < ((int32_t)7)))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Solitaire::SortDeckIntoTrips()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Solitaire_SortDeckIntoTrips_m6A43E9681644C41D6B7743B9B0B45A5066490ADB (Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mCFF42E67D10B26708DF415B8410EB546FC1611B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m966C074B573F7DC85DADF3000EE50742A42CA71D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mB63183A9151F4345A9DD444A7CBE0D6E03F77C7C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m21AEC50E791371101DC22ABCF96A2E46800811F8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_2 = NULL;
	int32_t V_3 = 0;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_4 = NULL;
	int32_t V_5 = 0;
	{
		// trips = deck.Count / 3;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_0 = __this->___deck_22;
		int32_t L_1;
		L_1 = List_1_get_Count_mB63183A9151F4345A9DD444A7CBE0D6E03F77C7C_inline(L_0, List_1_get_Count_mB63183A9151F4345A9DD444A7CBE0D6E03F77C7C_RuntimeMethod_var);
		__this->___trips_25 = ((int32_t)(L_1/3));
		// tripsRemainder = deck.Count % 3;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_2 = __this->___deck_22;
		int32_t L_3;
		L_3 = List_1_get_Count_mB63183A9151F4345A9DD444A7CBE0D6E03F77C7C_inline(L_2, List_1_get_Count_mB63183A9151F4345A9DD444A7CBE0D6E03F77C7C_RuntimeMethod_var);
		__this->___tripsRemainder_26 = ((int32_t)(L_3%3));
		// deckTrips.Clear();
		List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC* L_4 = __this->___deckTrips_14;
		List_1_Clear_m966C074B573F7DC85DADF3000EE50742A42CA71D_inline(L_4, List_1_Clear_m966C074B573F7DC85DADF3000EE50742A42CA71D_RuntimeMethod_var);
		// int modifier = 0;
		V_0 = 0;
		// for (int i = 0; i < trips; i++)
		V_1 = 0;
		goto IL_0071;
	}

IL_0037:
	{
		// List<string> myTrips = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_5 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_5, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_2 = L_5;
		// for (int j = 0; j < 3; j++)
		V_3 = 0;
		goto IL_0059;
	}

IL_0041:
	{
		// myTrips.Add(deck[j + modifier]);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_6 = V_2;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_7 = __this->___deck_22;
		int32_t L_8 = V_3;
		int32_t L_9 = V_0;
		String_t* L_10;
		L_10 = List_1_get_Item_m21AEC50E791371101DC22ABCF96A2E46800811F8(L_7, ((int32_t)il2cpp_codegen_add(L_8, L_9)), List_1_get_Item_m21AEC50E791371101DC22ABCF96A2E46800811F8_RuntimeMethod_var);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_6, L_10, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// for (int j = 0; j < 3; j++)
		int32_t L_11 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_11, 1));
	}

IL_0059:
	{
		// for (int j = 0; j < 3; j++)
		int32_t L_12 = V_3;
		if ((((int32_t)L_12) < ((int32_t)3)))
		{
			goto IL_0041;
		}
	}
	{
		// deckTrips.Add(myTrips);
		List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC* L_13 = __this->___deckTrips_14;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_14 = V_2;
		List_1_Add_mCFF42E67D10B26708DF415B8410EB546FC1611B5_inline(L_13, L_14, List_1_Add_mCFF42E67D10B26708DF415B8410EB546FC1611B5_RuntimeMethod_var);
		// modifier = modifier + 3;
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_15, 3));
		// for (int i = 0; i < trips; i++)
		int32_t L_16 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_16, 1));
	}

IL_0071:
	{
		// for (int i = 0; i < trips; i++)
		int32_t L_17 = V_1;
		int32_t L_18 = __this->___trips_25;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0037;
		}
	}
	{
		// if (tripsRemainder != 0)
		int32_t L_19 = __this->___tripsRemainder_26;
		if (!L_19)
		{
			goto IL_00e5;
		}
	}
	{
		// List<string> myRemainders = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_20 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_20, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_4 = L_20;
		// modifier = 0;
		V_0 = 0;
		// for (int k = 0; k < tripsRemainder; k++)
		V_5 = 0;
		goto IL_00c0;
	}

IL_0090:
	{
		// myRemainders.Add(deck[deck.Count - tripsRemainder + modifier]);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_21 = V_4;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_22 = __this->___deck_22;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_23 = __this->___deck_22;
		int32_t L_24;
		L_24 = List_1_get_Count_mB63183A9151F4345A9DD444A7CBE0D6E03F77C7C_inline(L_23, List_1_get_Count_mB63183A9151F4345A9DD444A7CBE0D6E03F77C7C_RuntimeMethod_var);
		int32_t L_25 = __this->___tripsRemainder_26;
		int32_t L_26 = V_0;
		String_t* L_27;
		L_27 = List_1_get_Item_m21AEC50E791371101DC22ABCF96A2E46800811F8(L_22, ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_subtract(L_24, L_25)), L_26)), List_1_get_Item_m21AEC50E791371101DC22ABCF96A2E46800811F8_RuntimeMethod_var);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_21, L_27, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// modifier++;
		int32_t L_28 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_28, 1));
		// for (int k = 0; k < tripsRemainder; k++)
		int32_t L_29 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add(L_29, 1));
	}

IL_00c0:
	{
		// for (int k = 0; k < tripsRemainder; k++)
		int32_t L_30 = V_5;
		int32_t L_31 = __this->___tripsRemainder_26;
		if ((((int32_t)L_30) < ((int32_t)L_31)))
		{
			goto IL_0090;
		}
	}
	{
		// deckTrips.Add(myRemainders);
		List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC* L_32 = __this->___deckTrips_14;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_33 = V_4;
		List_1_Add_mCFF42E67D10B26708DF415B8410EB546FC1611B5_inline(L_32, L_33, List_1_Add_mCFF42E67D10B26708DF415B8410EB546FC1611B5_RuntimeMethod_var);
		// trips++;
		int32_t L_34 = __this->___trips_25;
		__this->___trips_25 = ((int32_t)il2cpp_codegen_add(L_34, 1));
	}

IL_00e5:
	{
		// deckLocation = 0;
		__this->___deckLocation_24 = 0;
		// }
		return;
	}
}
// System.Void Solitaire::DealFromDeck()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Solitaire_DealFromDeck_m34AC6775F731EFFCF36FED448B595069B90709AB (Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_mD9E8CFB6777A99046B3C0195F7343FE771A2E99D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m7C02F7C6C6FF4FCF6B9C8DD17961EED6BE51B3FD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_m887DD9E8844786738857F4A68C25C7AADC55FFA0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3F21DCF0EA9BEEE61C939C25E335B1DB4C9EF9E);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_5;
	memset((&V_5), 0, sizeof(V_5));
	String_t* V_6 = NULL;
	{
		// foreach (Transform child in deckButton.transform)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___deckButton_6;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_1;
		L_1 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_0, NULL);
		RuntimeObject* L_2;
		L_2 = Transform_GetEnumerator_mA7E1C882ACA0C33E284711CD09971DEA3FFEF404(L_1, NULL);
		V_0 = L_2;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0064:
			{// begin finally (depth: 1)
				{
					RuntimeObject* L_3 = V_0;
					V_2 = ((RuntimeObject*)IsInst((RuntimeObject*)L_3, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var));
					RuntimeObject* L_4 = V_2;
					if (!L_4)
					{
						goto IL_0074;
					}
				}
				{
					RuntimeObject* L_5 = V_2;
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_5);
				}

IL_0074:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_005a_1;
			}

IL_0013_1:
			{
				// foreach (Transform child in deckButton.transform)
				RuntimeObject* L_6 = V_0;
				RuntimeObject* L_7;
				L_7 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_6);
				V_1 = ((Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1*)CastclassClass((RuntimeObject*)L_7, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_il2cpp_TypeInfo_var));
				// if (child.CompareTag("Card"))
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_8 = V_1;
				bool L_9;
				L_9 = Component_CompareTag_mE6F8897E84F12DF12D302FFC4D58204D51096FC5(L_8, _stringLiteralE3F21DCF0EA9BEEE61C939C25E335B1DB4C9EF9E, NULL);
				if (!L_9)
				{
					goto IL_005a_1;
				}
			}
			{
				// deck.Remove(child.name);
				List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_10 = __this->___deck_22;
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_11 = V_1;
				String_t* L_12;
				L_12 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_11, NULL);
				bool L_13;
				L_13 = List_1_Remove_mD9E8CFB6777A99046B3C0195F7343FE771A2E99D(L_10, L_12, List_1_Remove_mD9E8CFB6777A99046B3C0195F7343FE771A2E99D_RuntimeMethod_var);
				// discardPile.Add(child.name);
				List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_14 = __this->___discardPile_23;
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_15 = V_1;
				String_t* L_16;
				L_16 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_15, NULL);
				List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_14, L_16, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
				// Destroy(child.gameObject);
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_17 = V_1;
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_18;
				L_18 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_17, NULL);
				il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
				Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA(L_18, NULL);
			}

IL_005a_1:
			{
				// foreach (Transform child in deckButton.transform)
				RuntimeObject* L_19 = V_0;
				bool L_20;
				L_20 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_19);
				if (L_20)
				{
					goto IL_0013_1;
				}
			}
			{
				goto IL_0075;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0075:
	{
		// if (deckLocation < trips)
		int32_t L_21 = __this->___deckLocation_24;
		int32_t L_22 = __this->___trips_25;
		if ((((int32_t)L_21) >= ((int32_t)L_22)))
		{
			goto IL_0191;
		}
	}
	{
		// tripsOnDisplay.Clear();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_23 = __this->___tripsOnDisplay_13;
		List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_inline(L_23, List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_RuntimeMethod_var);
		// float xOffset = 1.2f;
		V_3 = (1.20000005f);
		// float zOffset = -0.2f;
		V_4 = (-0.200000003f);
		// foreach (string card in deckTrips[deckLocation])
		List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC* L_24 = __this->___deckTrips_14;
		int32_t L_25 = __this->___deckLocation_24;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_26;
		L_26 = List_1_get_Item_m7C02F7C6C6FF4FCF6B9C8DD17961EED6BE51B3FD(L_24, L_25, List_1_get_Item_m7C02F7C6C6FF4FCF6B9C8DD17961EED6BE51B3FD_RuntimeMethod_var);
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_27;
		L_27 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_26, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_5 = L_27;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0174:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_5), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0166_1;
			}

IL_00bb_1:
			{
				// foreach (string card in deckTrips[deckLocation])
				String_t* L_28;
				L_28 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_5), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_6 = L_28;
				// GameObject newTopCard = Instantiate(cardPrefab, new Vector3(deckButton.transform.position.x + xOffset, deckButton.transform.position.y, deckButton.transform.position.z + zOffset), Quaternion.identity, deckButton.transform);
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_29 = __this->___cardPrefab_5;
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_30 = __this->___deckButton_6;
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_31;
				L_31 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_30, NULL);
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32;
				L_32 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_31, NULL);
				float L_33 = L_32.___x_2;
				float L_34 = V_3;
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_35 = __this->___deckButton_6;
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_36;
				L_36 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_35, NULL);
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_37;
				L_37 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_36, NULL);
				float L_38 = L_37.___y_3;
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_39 = __this->___deckButton_6;
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_40;
				L_40 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_39, NULL);
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_41;
				L_41 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_40, NULL);
				float L_42 = L_41.___z_4;
				float L_43 = V_4;
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_44;
				memset((&L_44), 0, sizeof(L_44));
				Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_44), ((float)il2cpp_codegen_add(L_33, L_34)), L_38, ((float)il2cpp_codegen_add(L_42, L_43)), /*hidden argument*/NULL);
				Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_45;
				L_45 = Quaternion_get_identity_mB9CAEEB21BC81352CBF32DB9664BFC06FA7EA27B_inline(NULL);
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_46 = __this->___deckButton_6;
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_47;
				L_47 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_46, NULL);
				il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_48;
				L_48 = Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_m887DD9E8844786738857F4A68C25C7AADC55FFA0(L_29, L_44, L_45, L_47, Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_m887DD9E8844786738857F4A68C25C7AADC55FFA0_RuntimeMethod_var);
				// xOffset = xOffset + 0.5f;
				float L_49 = V_3;
				V_3 = ((float)il2cpp_codegen_add(L_49, (0.5f)));
				// zOffset = zOffset - 0.2f;
				float L_50 = V_4;
				V_4 = ((float)il2cpp_codegen_subtract(L_50, (0.200000003f)));
				// newTopCard.name = card;
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_51 = L_48;
				String_t* L_52 = V_6;
				Object_set_name_mC79E6DC8FFD72479C90F0C4CC7F42A0FEAF5AE47(L_51, L_52, NULL);
				// tripsOnDisplay.Add(card);
				List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_53 = __this->___tripsOnDisplay_13;
				String_t* L_54 = V_6;
				List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_53, L_54, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
				// newTopCard.GetComponent<Selectable>().faceUp = true;
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_55 = L_51;
				Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_56;
				L_56 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_55, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
				L_56->___faceUp_8 = (bool)1;
				// newTopCard.GetComponent<Selectable>().inDeckPile = true;
				Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_57;
				L_57 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_55, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
				L_57->___inDeckPile_9 = (bool)1;
			}

IL_0166_1:
			{
				// foreach (string card in deckTrips[deckLocation])
				bool L_58;
				L_58 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_5), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_58)
				{
					goto IL_00bb_1;
				}
			}
			{
				goto IL_0182;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0182:
	{
		// deckLocation++;
		int32_t L_59 = __this->___deckLocation_24;
		__this->___deckLocation_24 = ((int32_t)il2cpp_codegen_add(L_59, 1));
		return;
	}

IL_0191:
	{
		// RestackTopDeck();
		Solitaire_RestackTopDeck_m70300CC871C5AC743DB5C8A7234995430030C466(__this, NULL);
		// }
		return;
	}
}
// System.Void Solitaire::RestackTopDeck()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Solitaire_RestackTopDeck_m70300CC871C5AC743DB5C8A7234995430030C466 (Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_0;
	memset((&V_0), 0, sizeof(V_0));
	String_t* V_1 = NULL;
	{
		// deck.Clear();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_0 = __this->___deck_22;
		List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_inline(L_0, List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_RuntimeMethod_var);
		// foreach (string card in discardPile)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_1 = __this->___discardPile_23;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_2;
		L_2 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_1, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_0 = L_2;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0038:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_0), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_002d_1;
			}

IL_0019_1:
			{
				// foreach (string card in discardPile)
				String_t* L_3;
				L_3 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_0), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_1 = L_3;
				// deck.Add(card);
				List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_4 = __this->___deck_22;
				String_t* L_5 = V_1;
				List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_4, L_5, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
			}

IL_002d_1:
			{
				// foreach (string card in discardPile)
				bool L_6;
				L_6 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_0), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_6)
				{
					goto IL_0019_1;
				}
			}
			{
				goto IL_0046;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0046:
	{
		// discardPile.Clear();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_7 = __this->___discardPile_23;
		List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_inline(L_7, List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_RuntimeMethod_var);
		// SortDeckIntoTrips();
		Solitaire_SortDeckIntoTrips_m6A43E9681644C41D6B7743B9B0B45A5066490ADB(__this, NULL);
		// }
		return;
	}
}
// System.Void Solitaire::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Solitaire__ctor_m0129122DD857610D6D8B174913E3A5822FB0CFDA (Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m420EE34051D5B5A5E9EEA1DDF7817068C641CB8E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<string> tripsOnDisplay = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_0 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_0, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		__this->___tripsOnDisplay_13 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___tripsOnDisplay_13), (void*)L_0);
		// public List<List<string>> deckTrips = new List<List<string>>();
		List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC* L_1 = (List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC*)il2cpp_codegen_object_new(List_1_t73ED71AE73B40F21F5D8DAA1AD5207F8147562CC_il2cpp_TypeInfo_var);
		List_1__ctor_m420EE34051D5B5A5E9EEA1DDF7817068C641CB8E(L_1, List_1__ctor_m420EE34051D5B5A5E9EEA1DDF7817068C641CB8E_RuntimeMethod_var);
		__this->___deckTrips_14 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___deckTrips_14), (void*)L_1);
		// private List<string> bottom0 = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_2 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_2, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		__this->___bottom0_15 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___bottom0_15), (void*)L_2);
		// private List<string> bottom1 = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_3 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_3, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		__this->___bottom1_16 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___bottom1_16), (void*)L_3);
		// private List<string> bottom2 = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_4 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_4, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		__this->___bottom2_17 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___bottom2_17), (void*)L_4);
		// private List<string> bottom3 = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_5 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_5, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		__this->___bottom3_18 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___bottom3_18), (void*)L_5);
		// private List<string> bottom4 = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_6 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_6, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		__this->___bottom4_19 = L_6;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___bottom4_19), (void*)L_6);
		// private List<string> bottom5 = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_7 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_7, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		__this->___bottom5_20 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___bottom5_20), (void*)L_7);
		// private List<string> bottom6 = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_8 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_8, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		__this->___bottom6_21 = L_8;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___bottom6_21), (void*)L_8);
		// public List<string> discardPile = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_9 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_9, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		__this->___discardPile_23 = L_9;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___discardPile_23), (void*)L_9);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void Solitaire::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Solitaire__cctor_mE77011A998AE2030696238949035383AA594B6E8 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral09684B67A5909FD48E1F14A8AF8DDD483C620B10);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0EBD646B60E1C3FCE0203770591ED3C3D63537DC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2064F80F811DB79A33C4E51C10221454E30C74AE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral21ED4C7AF50D987589A9029FC0422151BE3A0FC2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral25D74BC981E6316A5E3CAEEA0BAF3C438F5C94DA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2F7234099CCD07F9C0939ACCC13D7F7F6E95DBAD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3D714DD3E8E77A697EF557E85ED2B014A96328C5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral410E5346BCA8EE150FFD507311DD85789F2E171E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral49A7EA21ECB328D154FA2262BB41626D795F4D90);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7402F76B2D6078A455F77607AF6E7350B6DE019E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7454695E25D304C65D0C1333D8008E862569CAE9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA019FB7F17AA36A9743C530E1F11D5613B8B1158);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAA4DA1A93F07E2E4B09F8D5E2C45905E26A45E79);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB16CF3324CA15FF0851B0F99DD86AC638C3E0CAE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBF86C9E9E7FE0EF09A2EAE8066CDC31F859254CC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEF420ABFDDBDA7B9EE665D85EF62E4A437554003);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static string[] suits = new string[] { "C", "D", "H", "S" };
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_0 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_1 = L_0;
		ArrayElementTypeCheck (L_1, _stringLiteralBF86C9E9E7FE0EF09A2EAE8066CDC31F859254CC);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralBF86C9E9E7FE0EF09A2EAE8066CDC31F859254CC);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_2 = L_1;
		ArrayElementTypeCheck (L_2, _stringLiteralA019FB7F17AA36A9743C530E1F11D5613B8B1158);
		(L_2)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteralA019FB7F17AA36A9743C530E1F11D5613B8B1158);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_3 = L_2;
		ArrayElementTypeCheck (L_3, _stringLiteral410E5346BCA8EE150FFD507311DD85789F2E171E);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral410E5346BCA8EE150FFD507311DD85789F2E171E);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_4 = L_3;
		ArrayElementTypeCheck (L_4, _stringLiteral09684B67A5909FD48E1F14A8AF8DDD483C620B10);
		(L_4)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral09684B67A5909FD48E1F14A8AF8DDD483C620B10);
		((Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_StaticFields*)il2cpp_codegen_static_fields_for(Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_il2cpp_TypeInfo_var))->___suits_9 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&((Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_StaticFields*)il2cpp_codegen_static_fields_for(Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_il2cpp_TypeInfo_var))->___suits_9), (void*)L_4);
		// public static string[] values = new string[] { "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)((int32_t)13));
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_6 = L_5;
		ArrayElementTypeCheck (L_6, _stringLiteralEF420ABFDDBDA7B9EE665D85EF62E4A437554003);
		(L_6)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralEF420ABFDDBDA7B9EE665D85EF62E4A437554003);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_7 = L_6;
		ArrayElementTypeCheck (L_7, _stringLiteral21ED4C7AF50D987589A9029FC0422151BE3A0FC2);
		(L_7)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral21ED4C7AF50D987589A9029FC0422151BE3A0FC2);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_8 = L_7;
		ArrayElementTypeCheck (L_8, _stringLiteral2064F80F811DB79A33C4E51C10221454E30C74AE);
		(L_8)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2064F80F811DB79A33C4E51C10221454E30C74AE);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_9 = L_8;
		ArrayElementTypeCheck (L_9, _stringLiteral7454695E25D304C65D0C1333D8008E862569CAE9);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral7454695E25D304C65D0C1333D8008E862569CAE9);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_10 = L_9;
		ArrayElementTypeCheck (L_10, _stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE);
		(L_10)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteralF7DDF59B44DDF9253B657C54053522CF694D3FBE);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_11 = L_10;
		ArrayElementTypeCheck (L_11, _stringLiteral2F7234099CCD07F9C0939ACCC13D7F7F6E95DBAD);
		(L_11)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(5), (String_t*)_stringLiteral2F7234099CCD07F9C0939ACCC13D7F7F6E95DBAD);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_12 = L_11;
		ArrayElementTypeCheck (L_12, _stringLiteral7402F76B2D6078A455F77607AF6E7350B6DE019E);
		(L_12)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral7402F76B2D6078A455F77607AF6E7350B6DE019E);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_13 = L_12;
		ArrayElementTypeCheck (L_13, _stringLiteralB16CF3324CA15FF0851B0F99DD86AC638C3E0CAE);
		(L_13)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(7), (String_t*)_stringLiteralB16CF3324CA15FF0851B0F99DD86AC638C3E0CAE);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_14 = L_13;
		ArrayElementTypeCheck (L_14, _stringLiteral25D74BC981E6316A5E3CAEEA0BAF3C438F5C94DA);
		(L_14)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(8), (String_t*)_stringLiteral25D74BC981E6316A5E3CAEEA0BAF3C438F5C94DA);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_15 = L_14;
		ArrayElementTypeCheck (L_15, _stringLiteralAA4DA1A93F07E2E4B09F8D5E2C45905E26A45E79);
		(L_15)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)9)), (String_t*)_stringLiteralAA4DA1A93F07E2E4B09F8D5E2C45905E26A45E79);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_16 = L_15;
		ArrayElementTypeCheck (L_16, _stringLiteral0EBD646B60E1C3FCE0203770591ED3C3D63537DC);
		(L_16)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)10)), (String_t*)_stringLiteral0EBD646B60E1C3FCE0203770591ED3C3D63537DC);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_17 = L_16;
		ArrayElementTypeCheck (L_17, _stringLiteral49A7EA21ECB328D154FA2262BB41626D795F4D90);
		(L_17)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)11)), (String_t*)_stringLiteral49A7EA21ECB328D154FA2262BB41626D795F4D90);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18 = L_17;
		ArrayElementTypeCheck (L_18, _stringLiteral3D714DD3E8E77A697EF557E85ED2B014A96328C5);
		(L_18)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)12)), (String_t*)_stringLiteral3D714DD3E8E77A697EF557E85ED2B014A96328C5);
		((Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_StaticFields*)il2cpp_codegen_static_fields_for(Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_il2cpp_TypeInfo_var))->___values_10 = L_18;
		Il2CppCodeGenWriteBarrier((void**)(&((Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_StaticFields*)il2cpp_codegen_static_fields_for(Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_il2cpp_TypeInfo_var))->___values_10), (void*)L_18);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Solitaire/<SolitaireDeal>d__28::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSolitaireDealU3Ed__28__ctor_m4853FC99373F6E44BEF3F1AEF9AB8AF90B6829B9 (U3CSolitaireDealU3Ed__28_t1551387CF05830C6119332636FEF237FE63DA94B* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void Solitaire/<SolitaireDeal>d__28::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSolitaireDealU3Ed__28_System_IDisposable_Dispose_m35614D5BEBE733B6F300892713EADCEA91F7D394 (U3CSolitaireDealU3Ed__28_t1551387CF05830C6119332636FEF237FE63DA94B* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)-3))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_001a;
		}
	}

IL_0010:
	{
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0013:
			{// begin finally (depth: 1)
				U3CSolitaireDealU3Ed__28_U3CU3Em__Finally1_mE37C5D2D2EEBDE3FF25159019FB5691FE02237AA(__this, NULL);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			goto IL_001a;
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean Solitaire/<SolitaireDeal>d__28::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CSolitaireDealU3Ed__28_MoveNext_mC28F87192A8F4206F1710D4320495772821D24B8 (U3CSolitaireDealU3Ed__28_t1551387CF05830C6119332636FEF237FE63DA94B* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_m359254483BE42CAD4DCA8FBAFB87473FB4CF00E1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_mD9E8CFB6777A99046B3C0195F7343FE771A2E99D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mB63183A9151F4345A9DD444A7CBE0D6E03F77C7C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m21AEC50E791371101DC22ABCF96A2E46800811F8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_m887DD9E8844786738857F4A68C25C7AADC55FFA0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* V_2 = NULL;
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* V_3 = NULL;
	int32_t V_4 = 0;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_5;
	memset((&V_5), 0, sizeof(V_5));
	String_t* V_6 = NULL;
	{
		auto __finallyBlock = il2cpp::utils::Fault([&]
		{

FAULT_0263:
			{// begin fault (depth: 1)
				U3CSolitaireDealU3Ed__28_System_IDisposable_Dispose_m35614D5BEBE733B6F300892713EADCEA91F7D394(__this, NULL);
				return;
			}// end fault
		});
		try
		{// begin try (depth: 1)
			{
				int32_t L_0 = __this->___U3CU3E1__state_0;
				V_1 = L_0;
				Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_1 = __this->___U3CU3E4__this_2;
				V_2 = L_1;
				int32_t L_2 = V_1;
				if (!L_2)
				{
					goto IL_001f_1;
				}
			}
			{
				int32_t L_3 = V_1;
				if ((((int32_t)L_3) == ((int32_t)1)))
				{
					goto IL_009c_1;
				}
			}
			{
				V_0 = (bool)0;
				goto IL_026a;
			}

IL_001f_1:
			{
				__this->___U3CU3E1__state_0 = (-1);
				// for (int i = 0; i < 7; i++)
				__this->___U3CiU3E5__2_3 = 0;
				goto IL_01fa_1;
			}

IL_0032_1:
			{
				// float yOffset = 0;
				__this->___U3CyOffsetU3E5__3_4 = (0.0f);
				// float zOffset = 0.03f;
				__this->___U3CzOffsetU3E5__4_5 = (0.0299999993f);
				// foreach (string card in bottoms[i])
				Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_4 = V_2;
				List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* L_5 = L_4->___bottoms_11;
				int32_t L_6 = __this->___U3CiU3E5__2_3;
				int32_t L_7 = L_6;
				List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_8 = (L_5)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_7));
				Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_9;
				L_9 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_8, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
				__this->___U3CU3E7__wrap4_6 = L_9;
				Il2CppCodeGenWriteBarrier((void**)&(((&__this->___U3CU3E7__wrap4_6))->____list_0), (void*)NULL);
				#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
				Il2CppCodeGenWriteBarrier((void**)&(((&__this->___U3CU3E7__wrap4_6))->____current_3), (void*)NULL);
				#endif
				__this->___U3CU3E1__state_0 = ((int32_t)-3);
				goto IL_01c6_1;
			}

IL_006d_1:
			{
				// foreach (string card in bottoms[i])
				Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1* L_10 = (&__this->___U3CU3E7__wrap4_6);
				String_t* L_11;
				L_11 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline(L_10, Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				__this->___U3CcardU3E5__6_7 = L_11;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CcardU3E5__6_7), (void*)L_11);
				// yield return new WaitForSeconds(0.05f);
				WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_12 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
				WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_12, (0.0500000007f), NULL);
				__this->___U3CU3E2__current_1 = L_12;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_12);
				__this->___U3CU3E1__state_0 = 1;
				V_0 = (bool)1;
				goto IL_026a;
			}

IL_009c_1:
			{
				__this->___U3CU3E1__state_0 = ((int32_t)-3);
				// GameObject newCard = Instantiate(cardPrefab, new Vector3(bottomPos[i].transform.position.x, bottomPos[i].transform.position.y - yOffset, bottomPos[i].transform.position.z - zOffset), Quaternion.identity, bottomPos[i].transform);
				Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_13 = V_2;
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_14 = L_13->___cardPrefab_5;
				Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_15 = V_2;
				GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_16 = L_15->___bottomPos_7;
				int32_t L_17 = __this->___U3CiU3E5__2_3;
				int32_t L_18 = L_17;
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_19 = (L_16)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_18));
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_20;
				L_20 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_19, NULL);
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_21;
				L_21 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_20, NULL);
				float L_22 = L_21.___x_2;
				Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_23 = V_2;
				GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_24 = L_23->___bottomPos_7;
				int32_t L_25 = __this->___U3CiU3E5__2_3;
				int32_t L_26 = L_25;
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_27 = (L_24)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_26));
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_28;
				L_28 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_27, NULL);
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_29;
				L_29 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_28, NULL);
				float L_30 = L_29.___y_3;
				float L_31 = __this->___U3CyOffsetU3E5__3_4;
				Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_32 = V_2;
				GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_33 = L_32->___bottomPos_7;
				int32_t L_34 = __this->___U3CiU3E5__2_3;
				int32_t L_35 = L_34;
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_36 = (L_33)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_35));
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_37;
				L_37 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_36, NULL);
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_38;
				L_38 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_37, NULL);
				float L_39 = L_38.___z_4;
				float L_40 = __this->___U3CzOffsetU3E5__4_5;
				Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_41;
				memset((&L_41), 0, sizeof(L_41));
				Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_41), L_22, ((float)il2cpp_codegen_subtract(L_30, L_31)), ((float)il2cpp_codegen_subtract(L_39, L_40)), /*hidden argument*/NULL);
				Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_42;
				L_42 = Quaternion_get_identity_mB9CAEEB21BC81352CBF32DB9664BFC06FA7EA27B_inline(NULL);
				Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_43 = V_2;
				GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_44 = L_43->___bottomPos_7;
				int32_t L_45 = __this->___U3CiU3E5__2_3;
				int32_t L_46 = L_45;
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_47 = (L_44)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_46));
				Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_48;
				L_48 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_47, NULL);
				il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_49;
				L_49 = Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_m887DD9E8844786738857F4A68C25C7AADC55FFA0(L_14, L_41, L_42, L_48, Object_Instantiate_TisGameObject_t76FEDD663AB33C991A9C9A23129337651094216F_m887DD9E8844786738857F4A68C25C7AADC55FFA0_RuntimeMethod_var);
				V_3 = L_49;
				// newCard.name = card;
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_50 = V_3;
				String_t* L_51 = __this->___U3CcardU3E5__6_7;
				Object_set_name_mC79E6DC8FFD72479C90F0C4CC7F42A0FEAF5AE47(L_50, L_51, NULL);
				// newCard.GetComponent<Selectable>().row = i;
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_52 = V_3;
				Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_53;
				L_53 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_52, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
				int32_t L_54 = __this->___U3CiU3E5__2_3;
				L_53->___row_7 = L_54;
				// if (card == bottoms[i][bottoms[i].Count -1])
				String_t* L_55 = __this->___U3CcardU3E5__6_7;
				Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_56 = V_2;
				List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* L_57 = L_56->___bottoms_11;
				int32_t L_58 = __this->___U3CiU3E5__2_3;
				int32_t L_59 = L_58;
				List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_60 = (L_57)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_59));
				Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_61 = V_2;
				List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* L_62 = L_61->___bottoms_11;
				int32_t L_63 = __this->___U3CiU3E5__2_3;
				int32_t L_64 = L_63;
				List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_65 = (L_62)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_64));
				int32_t L_66;
				L_66 = List_1_get_Count_mB63183A9151F4345A9DD444A7CBE0D6E03F77C7C_inline(L_65, List_1_get_Count_mB63183A9151F4345A9DD444A7CBE0D6E03F77C7C_RuntimeMethod_var);
				String_t* L_67;
				L_67 = List_1_get_Item_m21AEC50E791371101DC22ABCF96A2E46800811F8(L_60, ((int32_t)il2cpp_codegen_subtract(L_66, 1)), List_1_get_Item_m21AEC50E791371101DC22ABCF96A2E46800811F8_RuntimeMethod_var);
				bool L_68;
				L_68 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_55, L_67, NULL);
				if (!L_68)
				{
					goto IL_018a_1;
				}
			}
			{
				// newCard.GetComponent<Selectable>().faceUp = true;
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_69 = V_3;
				Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_70;
				L_70 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_69, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
				L_70->___faceUp_8 = (bool)1;
			}

IL_018a_1:
			{
				// yOffset = yOffset + 0.3f;
				float L_71 = __this->___U3CyOffsetU3E5__3_4;
				__this->___U3CyOffsetU3E5__3_4 = ((float)il2cpp_codegen_add(L_71, (0.300000012f)));
				// zOffset = zOffset + 0.03f;
				float L_72 = __this->___U3CzOffsetU3E5__4_5;
				__this->___U3CzOffsetU3E5__4_5 = ((float)il2cpp_codegen_add(L_72, (0.0299999993f)));
				// discardPile.Add(card);
				Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_73 = V_2;
				List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_74 = L_73->___discardPile_23;
				String_t* L_75 = __this->___U3CcardU3E5__6_7;
				List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_74, L_75, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
				// }
				__this->___U3CcardU3E5__6_7 = (String_t*)NULL;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CcardU3E5__6_7), (void*)(String_t*)NULL);
			}

IL_01c6_1:
			{
				// foreach (string card in bottoms[i])
				Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1* L_76 = (&__this->___U3CU3E7__wrap4_6);
				bool L_77;
				L_77 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED(L_76, Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_77)
				{
					goto IL_006d_1;
				}
			}
			{
				U3CSolitaireDealU3Ed__28_U3CU3Em__Finally1_mE37C5D2D2EEBDE3FF25159019FB5691FE02237AA(__this, NULL);
				Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1* L_78 = (&__this->___U3CU3E7__wrap4_6);
				il2cpp_codegen_initobj(L_78, sizeof(Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1));
				// for (int i = 0; i < 7; i++)
				int32_t L_79 = __this->___U3CiU3E5__2_3;
				V_4 = L_79;
				int32_t L_80 = V_4;
				__this->___U3CiU3E5__2_3 = ((int32_t)il2cpp_codegen_add(L_80, 1));
			}

IL_01fa_1:
			{
				// for (int i = 0; i < 7; i++)
				int32_t L_81 = __this->___U3CiU3E5__2_3;
				if ((((int32_t)L_81) < ((int32_t)7)))
				{
					goto IL_0032_1;
				}
			}
			{
				// foreach (string card in discardPile)
				Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_82 = V_2;
				List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_83 = L_82->___discardPile_23;
				Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_84;
				L_84 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_83, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
				V_5 = L_84;
			}
			{
				auto __finallyBlock = il2cpp::utils::Finally([&]
				{

FINALLY_0246_1:
					{// begin finally (depth: 2)
						Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_5), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
						return;
					}// end finally (depth: 2)
				});
				try
				{// begin try (depth: 2)
					{
						goto IL_023b_2;
					}

IL_0215_2:
					{
						// foreach (string card in discardPile)
						String_t* L_85;
						L_85 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_5), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
						V_6 = L_85;
						// if (deck.Contains(card))
						Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_86 = V_2;
						List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_87 = L_86->___deck_22;
						String_t* L_88 = V_6;
						bool L_89;
						L_89 = List_1_Contains_m359254483BE42CAD4DCA8FBAFB87473FB4CF00E1(L_87, L_88, List_1_Contains_m359254483BE42CAD4DCA8FBAFB87473FB4CF00E1_RuntimeMethod_var);
						if (!L_89)
						{
							goto IL_023b_2;
						}
					}
					{
						// deck.Remove(card);
						Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_90 = V_2;
						List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_91 = L_90->___deck_22;
						String_t* L_92 = V_6;
						bool L_93;
						L_93 = List_1_Remove_mD9E8CFB6777A99046B3C0195F7343FE771A2E99D(L_91, L_92, List_1_Remove_mD9E8CFB6777A99046B3C0195F7343FE771A2E99D_RuntimeMethod_var);
					}

IL_023b_2:
					{
						// foreach (string card in discardPile)
						bool L_94;
						L_94 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_5), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
						if (L_94)
						{
							goto IL_0215_2;
						}
					}
					{
						goto IL_0254_1;
					}
				}// end try (depth: 2)
				catch(Il2CppExceptionWrapper& e)
				{
					__finallyBlock.StoreException(e.ex);
				}
			}

IL_0254_1:
			{
				// discardPile.Clear();
				Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_95 = V_2;
				List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_96 = L_95->___discardPile_23;
				List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_inline(L_96, List_1_Clear_mC6C7AEBB0F980A717A87C0D12377984A464F0934_RuntimeMethod_var);
				// }
				V_0 = (bool)0;
				goto IL_026a;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_026a:
	{
		bool L_97 = V_0;
		return L_97;
	}
}
// System.Void Solitaire/<SolitaireDeal>d__28::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSolitaireDealU3Ed__28_U3CU3Em__Finally1_mE37C5D2D2EEBDE3FF25159019FB5691FE02237AA (U3CSolitaireDealU3Ed__28_t1551387CF05830C6119332636FEF237FE63DA94B* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->___U3CU3E1__state_0 = (-1);
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1* L_0 = (&__this->___U3CU3E7__wrap4_6);
		Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7(L_0, Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		return;
	}
}
// System.Object Solitaire/<SolitaireDeal>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CSolitaireDealU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCC1D7ECECACC853665B21D55A7F363EC1BE2C2E0 (U3CSolitaireDealU3Ed__28_t1551387CF05830C6119332636FEF237FE63DA94B* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void Solitaire/<SolitaireDeal>d__28::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSolitaireDealU3Ed__28_System_Collections_IEnumerator_Reset_m2B23C74A6DCDF1A9C0C25A801E8D3D2C4FC090BC (U3CSolitaireDealU3Ed__28_t1551387CF05830C6119332636FEF237FE63DA94B* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CSolitaireDealU3Ed__28_System_Collections_IEnumerator_Reset_m2B23C74A6DCDF1A9C0C25A801E8D3D2C4FC090BC_RuntimeMethod_var)));
	}
}
// System.Object Solitaire/<SolitaireDeal>d__28::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CSolitaireDealU3Ed__28_System_Collections_IEnumerator_get_Current_m44AB5AB5EAF8AF55792221CF6BF19E6F6271C729 (U3CSolitaireDealU3Ed__28_t1551387CF05830C6119332636FEF237FE63DA94B* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIButtons::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIButtons_Start_mCE48599A11D55FCCFAFD47F3805EEC21ECA2F951 (UIButtons_t4B44681F20665393EEF37CC4F0D19073DE59048E* __this, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Void UIButtons::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIButtons_Update_m8F0E7C758C89C37ED0B4B11A937C125BBDFA66A3 (UIButtons_t4B44681F20665393EEF37CC4F0D19073DE59048E* __this, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Void UIButtons::PlayAgain()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIButtons_PlayAgain_m2F4D930000DBF639DE2CF6D360FB171E53E73CC4 (UIButtons_t4B44681F20665393EEF37CC4F0D19073DE59048E* __this, const RuntimeMethod* method) 
{
	{
		// highScorePanel.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___highScorePanel_4;
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_0, (bool)0, NULL);
		// ResetScene();
		UIButtons_ResetScene_m12CD70C4CF320019CA5D036840D9E0A2F3CCC433(__this, NULL);
		// }
		return;
	}
}
// System.Void UIButtons::ResetScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIButtons_ResetScene_m12CD70C4CF320019CA5D036840D9E0A2F3CCC433 (UIButtons_t4B44681F20665393EEF37CC4F0D19073DE59048E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisSolitaire_t43D61047DE323D7D7559F84668F499D389CC1240_m7620A569538EA0B1B8B0F52835C0A76FCB017FF2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectsOfType_TisUpdateSprite_tDC00DA6185261B3C937D81EC2F99CDC5A9DEB83F_m2E109204B762B96BDA11780FA3909A47C8027B22_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	UpdateSpriteU5BU5D_tA884ABBF823CA957C7F6D74CD175979A907EBE93* V_0 = NULL;
	int32_t V_1 = 0;
	{
		// UpdateSprite[] cards = FindObjectsOfType<UpdateSprite>();
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		UpdateSpriteU5BU5D_tA884ABBF823CA957C7F6D74CD175979A907EBE93* L_0;
		L_0 = Object_FindObjectsOfType_TisUpdateSprite_tDC00DA6185261B3C937D81EC2F99CDC5A9DEB83F_m2E109204B762B96BDA11780FA3909A47C8027B22(Object_FindObjectsOfType_TisUpdateSprite_tDC00DA6185261B3C937D81EC2F99CDC5A9DEB83F_m2E109204B762B96BDA11780FA3909A47C8027B22_RuntimeMethod_var);
		// foreach (UpdateSprite card in cards)
		V_0 = L_0;
		V_1 = 0;
		goto IL_001b;
	}

IL_000a:
	{
		// foreach (UpdateSprite card in cards)
		UpdateSpriteU5BU5D_tA884ABBF823CA957C7F6D74CD175979A907EBE93* L_1 = V_0;
		int32_t L_2 = V_1;
		int32_t L_3 = L_2;
		UpdateSprite_tDC00DA6185261B3C937D81EC2F99CDC5A9DEB83F* L_4 = (L_1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_3));
		// Destroy(card.gameObject);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
		L_5 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_4, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA(L_5, NULL);
		int32_t L_6 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_6, 1));
	}

IL_001b:
	{
		// foreach (UpdateSprite card in cards)
		int32_t L_7 = V_1;
		UpdateSpriteU5BU5D_tA884ABBF823CA957C7F6D74CD175979A907EBE93* L_8 = V_0;
		if ((((int32_t)L_7) < ((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length)))))
		{
			goto IL_000a;
		}
	}
	{
		// ClearTopValues();
		UIButtons_ClearTopValues_m22554521111B98CD12C46FDF3EBFB171DF3A41A6(__this, NULL);
		// FindObjectOfType<Solitaire>().PlayCards();
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_9;
		L_9 = Object_FindObjectOfType_TisSolitaire_t43D61047DE323D7D7559F84668F499D389CC1240_m7620A569538EA0B1B8B0F52835C0A76FCB017FF2(Object_FindObjectOfType_TisSolitaire_t43D61047DE323D7D7559F84668F499D389CC1240_m7620A569538EA0B1B8B0F52835C0A76FCB017FF2_RuntimeMethod_var);
		Solitaire_PlayCards_m72D3A39228F204F5FDCAFA486A64D92459F2AFB0(L_9, NULL);
		// }
		return;
	}
}
// System.Void UIButtons::ClearTopValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIButtons_ClearTopValues_m22554521111B98CD12C46FDF3EBFB171DF3A41A6 (UIButtons_t4B44681F20665393EEF37CC4F0D19073DE59048E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectsOfType_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m83681642548ACBB0992224E09175AB18E0B65DBE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1690B3E0A7ABF26C7432995D1219914EE9822024);
		s_Il2CppMethodInitialized = true;
	}
	SelectableU5BU5D_t6B1728DC4B5C4A0E251A489FA114488E47166D0A* V_0 = NULL;
	int32_t V_1 = 0;
	Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* V_2 = NULL;
	{
		// Selectable[] selectables = FindObjectsOfType<Selectable>();
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		SelectableU5BU5D_t6B1728DC4B5C4A0E251A489FA114488E47166D0A* L_0;
		L_0 = Object_FindObjectsOfType_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m83681642548ACBB0992224E09175AB18E0B65DBE(Object_FindObjectsOfType_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m83681642548ACBB0992224E09175AB18E0B65DBE_RuntimeMethod_var);
		// foreach (Selectable selectable in selectables)
		V_0 = L_0;
		V_1 = 0;
		goto IL_002d;
	}

IL_000a:
	{
		// foreach (Selectable selectable in selectables)
		SelectableU5BU5D_t6B1728DC4B5C4A0E251A489FA114488E47166D0A* L_1 = V_0;
		int32_t L_2 = V_1;
		int32_t L_3 = L_2;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_4 = (L_1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = L_4;
		// if (selectable.CompareTag("Top"))
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_5 = V_2;
		bool L_6;
		L_6 = Component_CompareTag_mE6F8897E84F12DF12D302FFC4D58204D51096FC5(L_5, _stringLiteral1690B3E0A7ABF26C7432995D1219914EE9822024, NULL);
		if (!L_6)
		{
			goto IL_0029;
		}
	}
	{
		// selectable.suit = null;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_7 = V_2;
		L_7->___suit_5 = (String_t*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&L_7->___suit_5), (void*)(String_t*)NULL);
		// selectable.value = 0;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_8 = V_2;
		L_8->___value_6 = 0;
	}

IL_0029:
	{
		int32_t L_9 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_9, 1));
	}

IL_002d:
	{
		// foreach (Selectable selectable in selectables)
		int32_t L_10 = V_1;
		SelectableU5BU5D_t6B1728DC4B5C4A0E251A489FA114488E47166D0A* L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length)))))
		{
			goto IL_000a;
		}
	}
	{
		// }
		return;
	}
}
// System.Void UIButtons::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIButtons__ctor_mF3BA7757F4EE0871787B6C68BDDC2D3DCF905FF7 (UIButtons_t4B44681F20665393EEF37CC4F0D19073DE59048E* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UpdateSprite::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UpdateSprite_Start_m266DCAA8C866FA5D638AC7183B894D203C3ED802 (UpdateSprite_tDC00DA6185261B3C937D81EC2F99CDC5A9DEB83F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m8600576CD1B20D252F660EBD14F4BC39065A47B3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisSolitaire_t43D61047DE323D7D7559F84668F499D389CC1240_m7620A569538EA0B1B8B0F52835C0A76FCB017FF2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisUserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7_mB6FCD395BC8C50FE972E391CE11CC4DD9646B6D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_1;
	memset((&V_1), 0, sizeof(V_1));
	String_t* V_2 = NULL;
	{
		// List<string> deck = Solitaire.GenerateDeck();
		il2cpp_codegen_runtime_class_init_inline(Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240_il2cpp_TypeInfo_var);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_0;
		L_0 = Solitaire_GenerateDeck_mE430BEF13A823C578E35C2137BFA3A660BE22446(NULL);
		// solitaire = FindObjectOfType<Solitaire>();
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_1;
		L_1 = Object_FindObjectOfType_TisSolitaire_t43D61047DE323D7D7559F84668F499D389CC1240_m7620A569538EA0B1B8B0F52835C0A76FCB017FF2(Object_FindObjectOfType_TisSolitaire_t43D61047DE323D7D7559F84668F499D389CC1240_m7620A569538EA0B1B8B0F52835C0A76FCB017FF2_RuntimeMethod_var);
		__this->___solitaire_8 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___solitaire_8), (void*)L_1);
		// userInput = FindObjectOfType<UserInput>();
		UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* L_2;
		L_2 = Object_FindObjectOfType_TisUserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7_mB6FCD395BC8C50FE972E391CE11CC4DD9646B6D0(Object_FindObjectOfType_TisUserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7_mB6FCD395BC8C50FE972E391CE11CC4DD9646B6D0_RuntimeMethod_var);
		__this->___userInput_9 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___userInput_9), (void*)L_2);
		// int i = 0;
		V_0 = 0;
		// foreach (string card in deck)
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_3;
		L_3 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_0, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_1 = L_3;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_005f:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_1), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0054_1;
			}

IL_0025_1:
			{
				// foreach (string card in deck)
				String_t* L_4;
				L_4 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_1), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_2 = L_4;
				// if (this.name == card)
				String_t* L_5;
				L_5 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(__this, NULL);
				String_t* L_6 = V_2;
				bool L_7;
				L_7 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_5, L_6, NULL);
				if (!L_7)
				{
					goto IL_0050_1;
				}
			}
			{
				// cardFace = solitaire.cardFaces[i];
				Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_8 = __this->___solitaire_8;
				SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* L_9 = L_8->___cardFaces_4;
				int32_t L_10 = V_0;
				int32_t L_11 = L_10;
				Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* L_12 = (L_9)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_11));
				__this->___cardFace_4 = L_12;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___cardFace_4), (void*)L_12);
				// break;
				goto IL_006d;
			}

IL_0050_1:
			{
				// i++;
				int32_t L_13 = V_0;
				V_0 = ((int32_t)il2cpp_codegen_add(L_13, 1));
			}

IL_0054_1:
			{
				// foreach (string card in deck)
				bool L_14;
				L_14 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_1), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_14)
				{
					goto IL_0025_1;
				}
			}
			{
				goto IL_006d;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_006d:
	{
		// spriteRenderer = GetComponent<SpriteRenderer>();
		SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* L_15;
		L_15 = Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45(__this, Component_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m6181F10C09FC1650DAE0EF2308D344A2F170AA45_RuntimeMethod_var);
		__this->___spriteRenderer_6 = L_15;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___spriteRenderer_6), (void*)L_15);
		// selectable = GetComponent<Selectable>();
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_16;
		L_16 = Component_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m8600576CD1B20D252F660EBD14F4BC39065A47B3(__this, Component_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m8600576CD1B20D252F660EBD14F4BC39065A47B3_RuntimeMethod_var);
		__this->___selectable_7 = L_16;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___selectable_7), (void*)L_16);
		// }
		return;
	}
}
// System.Void UpdateSprite::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UpdateSprite_Update_m75EAD54828ED22075EA0489B30B8437863C6B43C (UpdateSprite_tDC00DA6185261B3C937D81EC2F99CDC5A9DEB83F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (selectable.faceUp == true)
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_0 = __this->___selectable_7;
		bool L_1 = L_0->___faceUp_8;
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		// spriteRenderer.sprite = cardFace;
		SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* L_2 = __this->___spriteRenderer_6;
		Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* L_3 = __this->___cardFace_4;
		SpriteRenderer_set_sprite_m7B176E33955108C60CAE21DFC153A0FAC674CB53(L_2, L_3, NULL);
		goto IL_0031;
	}

IL_0020:
	{
		// spriteRenderer.sprite = cardBack;
		SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* L_4 = __this->___spriteRenderer_6;
		Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* L_5 = __this->___cardBack_5;
		SpriteRenderer_set_sprite_m7B176E33955108C60CAE21DFC153A0FAC674CB53(L_4, L_5, NULL);
	}

IL_0031:
	{
		// if (userInput.slot1)
		UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* L_6 = __this->___userInput_9;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7 = L_6->___slot1_4;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Implicit_m18E1885C296CC868AC918101523697CFE6413C79(L_7, NULL);
		if (!L_8)
		{
			goto IL_0081;
		}
	}
	{
		// if (name == userInput.slot1.name)
		String_t* L_9;
		L_9 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(__this, NULL);
		UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* L_10 = __this->___userInput_9;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_11 = L_10->___slot1_4;
		String_t* L_12;
		L_12 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_11, NULL);
		bool L_13;
		L_13 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_9, L_12, NULL);
		if (!L_13)
		{
			goto IL_0071;
		}
	}
	{
		// spriteRenderer.color = Color.yellow;
		SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* L_14 = __this->___spriteRenderer_6;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_15;
		L_15 = Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline(NULL);
		SpriteRenderer_set_color_mB0EEC2845A0347E296C01C831F967731D2804546(L_14, L_15, NULL);
		return;
	}

IL_0071:
	{
		// spriteRenderer.color = Color.white;
		SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* L_16 = __this->___spriteRenderer_6;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_17;
		L_17 = Color_get_white_m28BB6E19F27D4EE6858D3021A44F62BC74E20C43_inline(NULL);
		SpriteRenderer_set_color_mB0EEC2845A0347E296C01C831F967731D2804546(L_16, L_17, NULL);
	}

IL_0081:
	{
		// }
		return;
	}
}
// System.Void UpdateSprite::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UpdateSprite__ctor_mD147FFD98ACF28F2A52DDAF980BE7B6F975DD908 (UpdateSprite_tDC00DA6185261B3C937D81EC2F99CDC5A9DEB83F* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UserInput::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInput_Start_mC73B8E0EE61359EC33603FD127DE524A70A9356F (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisSolitaire_t43D61047DE323D7D7559F84668F499D389CC1240_m7620A569538EA0B1B8B0F52835C0A76FCB017FF2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// solitaire = FindObjectOfType<Solitaire>();
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_0;
		L_0 = Object_FindObjectOfType_TisSolitaire_t43D61047DE323D7D7559F84668F499D389CC1240_m7620A569538EA0B1B8B0F52835C0A76FCB017FF2(Object_FindObjectOfType_TisSolitaire_t43D61047DE323D7D7559F84668F499D389CC1240_m7620A569538EA0B1B8B0F52835C0A76FCB017FF2_RuntimeMethod_var);
		__this->___solitaire_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___solitaire_5), (void*)L_0);
		// slot1 = this.gameObject;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		__this->___slot1_4 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___slot1_4), (void*)L_1);
		// }
		return;
	}
}
// System.Void UserInput::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInput_Update_m2BA028D4EC83095DFA9C6BF8E30434294465B6AF (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, const RuntimeMethod* method) 
{
	{
		// if (clickCount == 1)
		int32_t L_0 = __this->___clickCount_8;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_001b;
		}
	}
	{
		// timer += Time.deltaTime;
		float L_1 = __this->___timer_6;
		float L_2;
		L_2 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		__this->___timer_6 = ((float)il2cpp_codegen_add(L_1, L_2));
	}

IL_001b:
	{
		// if (clickCount == 3)
		int32_t L_3 = __this->___clickCount_8;
		if ((!(((uint32_t)L_3) == ((uint32_t)3))))
		{
			goto IL_0036;
		}
	}
	{
		// timer = 0;
		__this->___timer_6 = (0.0f);
		// clickCount = 1;
		__this->___clickCount_8 = 1;
	}

IL_0036:
	{
		// if (timer > doubleClickTime)
		float L_4 = __this->___timer_6;
		float L_5 = __this->___doubleClickTime_7;
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_0056;
		}
	}
	{
		// timer = 0;
		__this->___timer_6 = (0.0f);
		// clickCount = 0;
		__this->___clickCount_8 = 0;
	}

IL_0056:
	{
		// GetMouseClick();
		UserInput_GetMouseClick_m25267C57BDB8466106B107405074860BAD41E21C(__this, NULL);
		// }
		return;
	}
}
// System.Void UserInput::GetMouseClick()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInput_GetMouseClick_m25267C57BDB8466106B107405074860BAD41E21C (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1690B3E0A7ABF26C7432995D1219914EE9822024);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB157F89A1C7FC50EFF8E1244B8DB0FF3A13F5118);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3F21DCF0EA9BEEE61C939C25E335B1DB4C9EF9E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE9B577CAE7605B964C63995C672D6F87AF3CAF63);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (Input.GetMouseButtonDown(0))
		bool L_0;
		L_0 = Input_GetMouseButtonDown_m33522C56A54C402FE6DED802DD7E53435C27A5DE(0, NULL);
		if (!L_0)
		{
			goto IL_00f7;
		}
	}
	{
		// clickCount++;
		int32_t L_1 = __this->___clickCount_8;
		__this->___clickCount_8 = ((int32_t)il2cpp_codegen_add(L_1, 1));
		// Vector3 mousePosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -10));
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_2;
		L_2 = Camera_get_main_mF222B707D3BF8CC9C7544609EFC71CFB62E81D43(NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Input_get_mousePosition_m2414B43222ED0C5FAB960D393964189AFD21EEAD(NULL);
		float L_4 = L_3.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5;
		L_5 = Input_get_mousePosition_m2414B43222ED0C5FAB960D393964189AFD21EEAD(NULL);
		float L_6 = L_5.___y_3;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		memset((&L_7), 0, sizeof(L_7));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_7), L_4, L_6, (-10.0f), /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_8;
		L_8 = Camera_ScreenToWorldPoint_m5EA3148F070985EC72127AAC3448D8D6ABE6E7E5(L_2, L_7, NULL);
		// RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_9;
		L_9 = Camera_get_main_mF222B707D3BF8CC9C7544609EFC71CFB62E81D43(NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10;
		L_10 = Input_get_mousePosition_m2414B43222ED0C5FAB960D393964189AFD21EEAD(NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_11;
		L_11 = Camera_ScreenToWorldPoint_m5EA3148F070985EC72127AAC3448D8D6ABE6E7E5(L_9, L_10, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_12;
		L_12 = Vector2_op_Implicit_m8F73B300CB4E6F9B4EB5FB6130363D76CEAA230B_inline(L_11, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_13;
		L_13 = Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline(NULL);
		il2cpp_codegen_runtime_class_init_inline(Physics2D_t64C0DB5246067DAC2E83A52558A0AC68AF3BE94D_il2cpp_TypeInfo_var);
		RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA L_14;
		L_14 = Physics2D_Raycast_m8D056DA6FCBB18FD6F64BA247C16E050B0DBBF18(L_12, L_13, NULL);
		V_0 = L_14;
		// if (hit)
		RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA L_15 = V_0;
		bool L_16;
		L_16 = RaycastHit2D_op_Implicit_m768ECEE43BC378B51CB2304A9D547E7683CC3EE5(L_15, NULL);
		if (!L_16)
		{
			goto IL_00f7;
		}
	}
	{
		// if (hit.collider.CompareTag("Deck"))
		Collider2D_t6A17BA7734600EF3F26588E9ED903617D5B8EB52* L_17;
		L_17 = RaycastHit2D_get_collider_mB56DFCD16B708852EEBDBB490BC8665DBF7487FD((&V_0), NULL);
		bool L_18;
		L_18 = Component_CompareTag_mE6F8897E84F12DF12D302FFC4D58204D51096FC5(L_17, _stringLiteralE9B577CAE7605B964C63995C672D6F87AF3CAF63, NULL);
		if (!L_18)
		{
			goto IL_0086;
		}
	}
	{
		// Deck();
		UserInput_Deck_m81436BDC023279A2DF8FC3C7B07C18DEB6BAA7A4(__this, NULL);
		return;
	}

IL_0086:
	{
		// else if (hit.collider.CompareTag("Card"))
		Collider2D_t6A17BA7734600EF3F26588E9ED903617D5B8EB52* L_19;
		L_19 = RaycastHit2D_get_collider_mB56DFCD16B708852EEBDBB490BC8665DBF7487FD((&V_0), NULL);
		bool L_20;
		L_20 = Component_CompareTag_mE6F8897E84F12DF12D302FFC4D58204D51096FC5(L_19, _stringLiteralE3F21DCF0EA9BEEE61C939C25E335B1DB4C9EF9E, NULL);
		if (!L_20)
		{
			goto IL_00ac;
		}
	}
	{
		// Card(hit.collider.gameObject);
		Collider2D_t6A17BA7734600EF3F26588E9ED903617D5B8EB52* L_21;
		L_21 = RaycastHit2D_get_collider_mB56DFCD16B708852EEBDBB490BC8665DBF7487FD((&V_0), NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_22;
		L_22 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_21, NULL);
		UserInput_Card_m61BB20A7E54456DBE7881A622A3FCE4E7A1C001F(__this, L_22, NULL);
		return;
	}

IL_00ac:
	{
		// else if (hit.collider.CompareTag("Top"))
		Collider2D_t6A17BA7734600EF3F26588E9ED903617D5B8EB52* L_23;
		L_23 = RaycastHit2D_get_collider_mB56DFCD16B708852EEBDBB490BC8665DBF7487FD((&V_0), NULL);
		bool L_24;
		L_24 = Component_CompareTag_mE6F8897E84F12DF12D302FFC4D58204D51096FC5(L_23, _stringLiteral1690B3E0A7ABF26C7432995D1219914EE9822024, NULL);
		if (!L_24)
		{
			goto IL_00d2;
		}
	}
	{
		// Top(hit.collider.gameObject);
		Collider2D_t6A17BA7734600EF3F26588E9ED903617D5B8EB52* L_25;
		L_25 = RaycastHit2D_get_collider_mB56DFCD16B708852EEBDBB490BC8665DBF7487FD((&V_0), NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_26;
		L_26 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_25, NULL);
		UserInput_Top_m6D128BE13556EC650D464A52115774A60646862E(__this, L_26, NULL);
		return;
	}

IL_00d2:
	{
		// else if (hit.collider.CompareTag("Bottom"))
		Collider2D_t6A17BA7734600EF3F26588E9ED903617D5B8EB52* L_27;
		L_27 = RaycastHit2D_get_collider_mB56DFCD16B708852EEBDBB490BC8665DBF7487FD((&V_0), NULL);
		bool L_28;
		L_28 = Component_CompareTag_mE6F8897E84F12DF12D302FFC4D58204D51096FC5(L_27, _stringLiteralB157F89A1C7FC50EFF8E1244B8DB0FF3A13F5118, NULL);
		if (!L_28)
		{
			goto IL_00f7;
		}
	}
	{
		// Bottom(hit.collider.gameObject);
		Collider2D_t6A17BA7734600EF3F26588E9ED903617D5B8EB52* L_29;
		L_29 = RaycastHit2D_get_collider_mB56DFCD16B708852EEBDBB490BC8665DBF7487FD((&V_0), NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_30;
		L_30 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_29, NULL);
		UserInput_Bottom_m52CFBCBDEC44D99AB54CAB9C14B986085AF41C69(__this, L_30, NULL);
	}

IL_00f7:
	{
		// }
		return;
	}
}
// System.Void UserInput::Deck()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInput_Deck_m81436BDC023279A2DF8FC3C7B07C18DEB6BAA7A4 (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6C64D1112BBF8B2A09059130C37F2ED556ECEC47);
		s_Il2CppMethodInitialized = true;
	}
	{
		// print("Clicked on deck");
		MonoBehaviour_print_mED815C779E369787B3E9646A6DE96FBC2944BF0B(_stringLiteral6C64D1112BBF8B2A09059130C37F2ED556ECEC47, NULL);
		// solitaire.DealFromDeck();
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_0 = __this->___solitaire_5;
		Solitaire_DealFromDeck_m34AC6775F731EFFCF36FED448B595069B90709AB(L_0, NULL);
		// slot1 = this.gameObject;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		__this->___slot1_4 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___slot1_4), (void*)L_1);
		// }
		return;
	}
}
// System.Void UserInput::Card(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInput_Card_m61BB20A7E54456DBE7881A622A3FCE4E7A1C001F (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___selected0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD14EAC0D0010CD8B317D8E85A95D5E259525B9AC);
		s_Il2CppMethodInitialized = true;
	}
	{
		// print("Clicked on Card");
		MonoBehaviour_print_mED815C779E369787B3E9646A6DE96FBC2944BF0B(_stringLiteralD14EAC0D0010CD8B317D8E85A95D5E259525B9AC, NULL);
		// if (!selected.GetComponent<Selectable>().faceUp) // if the card clicked on is facedown
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = ___selected0;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_1;
		L_1 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_0, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		bool L_2 = L_1->___faceUp_8;
		if (L_2)
		{
			goto IL_003c;
		}
	}
	{
		// if (!Blocked(selected)) // if the card clicked on is not blocked
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = ___selected0;
		bool L_4;
		L_4 = UserInput_Blocked_m4830DFF904DB15168334ACD2991538AF1F7ECD18(__this, L_3, NULL);
		if (L_4)
		{
			goto IL_00da;
		}
	}
	{
		// selected.GetComponent<Selectable>().faceUp = true;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = ___selected0;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_6;
		L_6 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_5, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		L_6->___faceUp_8 = (bool)1;
		// slot1 = this.gameObject;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7;
		L_7 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		__this->___slot1_4 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___slot1_4), (void*)L_7);
		return;
	}

IL_003c:
	{
		// else if (selected.GetComponent<Selectable>().inDeckPile) // if the card clicked on is in the deck pile with the trips
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_8 = ___selected0;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_9;
		L_9 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_8, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		bool L_10 = L_9->___inDeckPile_9;
		if (!L_10)
		{
			goto IL_007b;
		}
	}
	{
		// if (!Blocked(selected))
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_11 = ___selected0;
		bool L_12;
		L_12 = UserInput_Blocked_m4830DFF904DB15168334ACD2991538AF1F7ECD18(__this, L_11, NULL);
		if (L_12)
		{
			goto IL_00da;
		}
	}
	{
		// if (slot1 == selected) // if the same card is clicked twice
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13 = __this->___slot1_4;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_14 = ___selected0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_15;
		L_15 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_13, L_14, NULL);
		if (!L_15)
		{
			goto IL_0073;
		}
	}
	{
		// if (DoubleClick())
		bool L_16;
		L_16 = UserInput_DoubleClick_m5CE515EA8503B13815F9515D4899936F36935CF9(__this, NULL);
		if (!L_16)
		{
			goto IL_00da;
		}
	}
	{
		// AutoStack(selected);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_17 = ___selected0;
		UserInput_AutoStack_mE4DC3420E73DE955AE6987264F0DFD9FD8861B8B(__this, L_17, NULL);
		return;
	}

IL_0073:
	{
		// slot1 = selected;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_18 = ___selected0;
		__this->___slot1_4 = L_18;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___slot1_4), (void*)L_18);
		return;
	}

IL_007b:
	{
		// if (slot1 == this.gameObject) // not null because we pass in this gameObject instead
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_19 = __this->___slot1_4;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_20;
		L_20 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_21;
		L_21 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_19, L_20, NULL);
		if (!L_21)
		{
			goto IL_0096;
		}
	}
	{
		// slot1 = selected;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_22 = ___selected0;
		__this->___slot1_4 = L_22;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___slot1_4), (void*)L_22);
		return;
	}

IL_0096:
	{
		// else if (slot1 != selected)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_23 = __this->___slot1_4;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_24 = ___selected0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_25;
		L_25 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_23, L_24, NULL);
		if (!L_25)
		{
			goto IL_00bd;
		}
	}
	{
		// if (Stackable(selected))
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_26 = ___selected0;
		bool L_27;
		L_27 = UserInput_Stackable_m8F68A76920F4753B849A7FE5C7655711C0BD4BD2(__this, L_26, NULL);
		if (!L_27)
		{
			goto IL_00b5;
		}
	}
	{
		// Stack(selected);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_28 = ___selected0;
		UserInput_Stack_m072D819EC1B4D6C4A15DDCCF3F121CE90E4695B7(__this, L_28, NULL);
		return;
	}

IL_00b5:
	{
		// slot1 = selected;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_29 = ___selected0;
		__this->___slot1_4 = L_29;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___slot1_4), (void*)L_29);
		return;
	}

IL_00bd:
	{
		// else if (slot1 == selected) // if the same card is clicked twice
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_30 = __this->___slot1_4;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_31 = ___selected0;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_32;
		L_32 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_30, L_31, NULL);
		if (!L_32)
		{
			goto IL_00da;
		}
	}
	{
		// if (DoubleClick())
		bool L_33;
		L_33 = UserInput_DoubleClick_m5CE515EA8503B13815F9515D4899936F36935CF9(__this, NULL);
		if (!L_33)
		{
			goto IL_00da;
		}
	}
	{
		// AutoStack(selected);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_34 = ___selected0;
		UserInput_AutoStack_mE4DC3420E73DE955AE6987264F0DFD9FD8861B8B(__this, L_34, NULL);
	}

IL_00da:
	{
		// }
		return;
	}
}
// System.Void UserInput::Top(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInput_Top_m6D128BE13556EC650D464A52115774A60646862E (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___selected0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE0EF57155BBB9B198D7E7295CFF8A209DE3D0008);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3F21DCF0EA9BEEE61C939C25E335B1DB4C9EF9E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// print("Clicked on Top");
		MonoBehaviour_print_mED815C779E369787B3E9646A6DE96FBC2944BF0B(_stringLiteralE0EF57155BBB9B198D7E7295CFF8A209DE3D0008, NULL);
		// if (slot1.CompareTag("Card"))
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___slot1_4;
		bool L_1;
		L_1 = GameObject_CompareTag_m6378BE50D009A93D46036F74CC3F7E2ECB0636E5(L_0, _stringLiteralE3F21DCF0EA9BEEE61C939C25E335B1DB4C9EF9E, NULL);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		// if (slot1.GetComponent<Selectable>().value == 1)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___slot1_4;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_3;
		L_3 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_2, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		int32_t L_4 = L_3->___value_6;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0036;
		}
	}
	{
		// Stack(selected);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = ___selected0;
		UserInput_Stack_m072D819EC1B4D6C4A15DDCCF3F121CE90E4695B7(__this, L_5, NULL);
	}

IL_0036:
	{
		// }
		return;
	}
}
// System.Void UserInput::Bottom(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInput_Bottom_m52CFBCBDEC44D99AB54CAB9C14B986085AF41C69 (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___selected0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9DAB8995495FBD15B7CFC865BE4B64C25615CCBF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3F21DCF0EA9BEEE61C939C25E335B1DB4C9EF9E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// print("Clicked on Bottom");
		MonoBehaviour_print_mED815C779E369787B3E9646A6DE96FBC2944BF0B(_stringLiteral9DAB8995495FBD15B7CFC865BE4B64C25615CCBF, NULL);
		// if (slot1.CompareTag("Card"))
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___slot1_4;
		bool L_1;
		L_1 = GameObject_CompareTag_m6378BE50D009A93D46036F74CC3F7E2ECB0636E5(L_0, _stringLiteralE3F21DCF0EA9BEEE61C939C25E335B1DB4C9EF9E, NULL);
		if (!L_1)
		{
			goto IL_0037;
		}
	}
	{
		// if (slot1.GetComponent<Selectable>().value == 13)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___slot1_4;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_3;
		L_3 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_2, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		int32_t L_4 = L_3->___value_6;
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0037;
		}
	}
	{
		// Stack(selected);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = ___selected0;
		UserInput_Stack_m072D819EC1B4D6C4A15DDCCF3F121CE90E4695B7(__this, L_5, NULL);
	}

IL_0037:
	{
		// }
		return;
	}
}
// System.Boolean UserInput::Stackable(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UserInput_Stackable_m8F68A76920F4753B849A7FE5C7655711C0BD4BD2 (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___selected0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral09684B67A5909FD48E1F14A8AF8DDD483C620B10);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4DE7F5C690036C169C4E241BCAC36102AEE31873);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBF86C9E9E7FE0EF09A2EAE8066CDC31F859254CC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC92E1C3CC8E3494BBF77478799B4D0624331268E);
		s_Il2CppMethodInitialized = true;
	}
	Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* V_0 = NULL;
	Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* V_1 = NULL;
	bool V_2 = false;
	bool V_3 = false;
	{
		// Selectable s1 = slot1.GetComponent<Selectable>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___slot1_4;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_1;
		L_1 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_0, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		V_0 = L_1;
		// Selectable s2 = selected.GetComponent<Selectable>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = ___selected0;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_3;
		L_3 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_2, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		V_1 = L_3;
		// if (!s2.inDeckPile)
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_4 = V_1;
		bool L_5 = L_4->___inDeckPile_9;
		if (L_5)
		{
			goto IL_00dd;
		}
	}
	{
		// if (s2.top) // if in the top pile must stack suited Ace to King
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_6 = V_1;
		bool L_7 = L_6->___top_4;
		if (!L_7)
		{
			goto IL_0061;
		}
	}
	{
		// if (s1.suit == s2.suit || (s1.value == 1 && s2.suit == null))
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_8 = V_0;
		String_t* L_9 = L_8->___suit_5;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_10 = V_1;
		String_t* L_11 = L_10->___suit_5;
		bool L_12;
		L_12 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_9, L_11, NULL);
		if (L_12)
		{
			goto IL_004a;
		}
	}
	{
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_13 = V_0;
		int32_t L_14 = L_13->___value_6;
		if ((!(((uint32_t)L_14) == ((uint32_t)1))))
		{
			goto IL_005f;
		}
	}
	{
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_15 = V_1;
		String_t* L_16 = L_15->___suit_5;
		if (L_16)
		{
			goto IL_005f;
		}
	}

IL_004a:
	{
		// if (s1.value == s2.value + 1)
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_17 = V_0;
		int32_t L_18 = L_17->___value_6;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_19 = V_1;
		int32_t L_20 = L_19->___value_6;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)il2cpp_codegen_add(L_20, 1))))))
		{
			goto IL_00dd;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_005f:
	{
		// return false;
		return (bool)0;
	}

IL_0061:
	{
		// if (s1.value == s2.value - 1)
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_21 = V_0;
		int32_t L_22 = L_21->___value_6;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_23 = V_1;
		int32_t L_24 = L_23->___value_6;
		if ((!(((uint32_t)L_22) == ((uint32_t)((int32_t)il2cpp_codegen_subtract(L_24, 1))))))
		{
			goto IL_00dd;
		}
	}
	{
		// bool card1Red = true;
		V_2 = (bool)1;
		// bool card2Red = true;
		V_3 = (bool)1;
		// if (s1.suit == "C" || s1.suit == "S")
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_25 = V_0;
		String_t* L_26 = L_25->___suit_5;
		bool L_27;
		L_27 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_26, _stringLiteralBF86C9E9E7FE0EF09A2EAE8066CDC31F859254CC, NULL);
		if (L_27)
		{
			goto IL_0099;
		}
	}
	{
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_28 = V_0;
		String_t* L_29 = L_28->___suit_5;
		bool L_30;
		L_30 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_29, _stringLiteral09684B67A5909FD48E1F14A8AF8DDD483C620B10, NULL);
		if (!L_30)
		{
			goto IL_009b;
		}
	}

IL_0099:
	{
		// card1Red = false;
		V_2 = (bool)0;
	}

IL_009b:
	{
		// if (s2.suit == "C" || s2.suit == "S")
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_31 = V_1;
		String_t* L_32 = L_31->___suit_5;
		bool L_33;
		L_33 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_32, _stringLiteralBF86C9E9E7FE0EF09A2EAE8066CDC31F859254CC, NULL);
		if (L_33)
		{
			goto IL_00bf;
		}
	}
	{
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_34 = V_1;
		String_t* L_35 = L_34->___suit_5;
		bool L_36;
		L_36 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_35, _stringLiteral09684B67A5909FD48E1F14A8AF8DDD483C620B10, NULL);
		if (!L_36)
		{
			goto IL_00c1;
		}
	}

IL_00bf:
	{
		// card2Red = false;
		V_3 = (bool)0;
	}

IL_00c1:
	{
		// if (card1Red == card2Red)
		bool L_37 = V_2;
		bool L_38 = V_3;
		if ((!(((uint32_t)L_37) == ((uint32_t)L_38))))
		{
			goto IL_00d1;
		}
	}
	{
		// print("Not stackable");
		MonoBehaviour_print_mED815C779E369787B3E9646A6DE96FBC2944BF0B(_stringLiteral4DE7F5C690036C169C4E241BCAC36102AEE31873, NULL);
		// return false;
		return (bool)0;
	}

IL_00d1:
	{
		// print("Stackable");
		MonoBehaviour_print_mED815C779E369787B3E9646A6DE96FBC2944BF0B(_stringLiteralC92E1C3CC8E3494BBF77478799B4D0624331268E, NULL);
		// return true;
		return (bool)1;
	}

IL_00dd:
	{
		// return false;
		return (bool)0;
	}
}
// System.Void UserInput::Stack(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInput_Stack_m072D819EC1B4D6C4A15DDCCF3F121CE90E4695B7 (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___selected0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_mD9E8CFB6777A99046B3C0195F7343FE771A2E99D_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* V_0 = NULL;
	Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* V_1 = NULL;
	float V_2 = 0.0f;
	{
		// Selectable s1 = slot1.GetComponent<Selectable>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___slot1_4;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_1;
		L_1 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_0, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		V_0 = L_1;
		// Selectable s2 = selected.GetComponent<Selectable>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = ___selected0;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_3;
		L_3 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_2, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		V_1 = L_3;
		// float yOffset = 0.3f;
		V_2 = (0.300000012f);
		// if (s2.top || (!s2.top && s1.value == 13))
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_4 = V_1;
		bool L_5 = L_4->___top_4;
		if (L_5)
		{
			goto IL_0033;
		}
	}
	{
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_6 = V_1;
		bool L_7 = L_6->___top_4;
		if (L_7)
		{
			goto IL_0039;
		}
	}
	{
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_8 = V_0;
		int32_t L_9 = L_8->___value_6;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0039;
		}
	}

IL_0033:
	{
		// yOffset = 0;
		V_2 = (0.0f);
	}

IL_0039:
	{
		// slot1.transform.position = new Vector3(selected.transform.position.x, selected.transform.position.y - yOffset, selected.transform.position.z - 0.01f);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10 = __this->___slot1_4;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_11;
		L_11 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_10, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = ___selected0;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_13;
		L_13 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_12, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_14;
		L_14 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_13, NULL);
		float L_15 = L_14.___x_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_16 = ___selected0;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_17;
		L_17 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_16, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_18;
		L_18 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_17, NULL);
		float L_19 = L_18.___y_3;
		float L_20 = V_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_21 = ___selected0;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_22;
		L_22 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_21, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_23;
		L_23 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_22, NULL);
		float L_24 = L_23.___z_4;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_25;
		memset((&L_25), 0, sizeof(L_25));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_25), L_15, ((float)il2cpp_codegen_subtract(L_19, L_20)), ((float)il2cpp_codegen_subtract(L_24, (0.00999999978f))), /*hidden argument*/NULL);
		Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156(L_11, L_25, NULL);
		// slot1.transform.parent = selected.transform; // this makes the children move with the parents
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_26 = __this->___slot1_4;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_27;
		L_27 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_26, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_28 = ___selected0;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_29;
		L_29 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_28, NULL);
		Transform_set_parent_m9BD5E563B539DD5BEC342736B03F97B38A243234(L_27, L_29, NULL);
		// if (s1.inDeckPile) // removes the cards from the top pile to prevent duplicate cards
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_30 = V_0;
		bool L_31 = L_30->___inDeckPile_9;
		if (!L_31)
		{
			goto IL_00c5;
		}
	}
	{
		// solitaire.tripsOnDisplay.Remove(slot1.name);
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_32 = __this->___solitaire_5;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_33 = L_32->___tripsOnDisplay_13;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_34 = __this->___slot1_4;
		String_t* L_35;
		L_35 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_34, NULL);
		bool L_36;
		L_36 = List_1_Remove_mD9E8CFB6777A99046B3C0195F7343FE771A2E99D(L_33, L_35, List_1_Remove_mD9E8CFB6777A99046B3C0195F7343FE771A2E99D_RuntimeMethod_var);
		goto IL_016b;
	}

IL_00c5:
	{
		// else if (s1.top && s2.top && s1.value == 1) // allows movement of cards between top spots
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_37 = V_0;
		bool L_38 = L_37->___top_4;
		if (!L_38)
		{
			goto IL_011a;
		}
	}
	{
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_39 = V_1;
		bool L_40 = L_39->___top_4;
		if (!L_40)
		{
			goto IL_011a;
		}
	}
	{
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_41 = V_0;
		int32_t L_42 = L_41->___value_6;
		if ((!(((uint32_t)L_42) == ((uint32_t)1))))
		{
			goto IL_011a;
		}
	}
	{
		// solitaire.topPos[s1.row].GetComponent<Selectable>().value = 0;
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_43 = __this->___solitaire_5;
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_44 = L_43->___topPos_8;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_45 = V_0;
		int32_t L_46 = L_45->___row_7;
		int32_t L_47 = L_46;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_48 = (L_44)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_47));
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_49;
		L_49 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_48, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		L_49->___value_6 = 0;
		// solitaire.topPos[s1.row].GetComponent<Selectable>().suit = null;
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_50 = __this->___solitaire_5;
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_51 = L_50->___topPos_8;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_52 = V_0;
		int32_t L_53 = L_52->___row_7;
		int32_t L_54 = L_53;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_55 = (L_51)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_54));
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_56;
		L_56 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_55, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		L_56->___suit_5 = (String_t*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&L_56->___suit_5), (void*)(String_t*)NULL);
		goto IL_016b;
	}

IL_011a:
	{
		// else if (s1.top) // keeps track of the current value of the top decks as a card has been removed
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_57 = V_0;
		bool L_58 = L_57->___top_4;
		if (!L_58)
		{
			goto IL_0148;
		}
	}
	{
		// solitaire.topPos[s1.row].GetComponent<Selectable>().value = s1.value - 1;
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_59 = __this->___solitaire_5;
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_60 = L_59->___topPos_8;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_61 = V_0;
		int32_t L_62 = L_61->___row_7;
		int32_t L_63 = L_62;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_64 = (L_60)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_63));
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_65;
		L_65 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_64, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_66 = V_0;
		int32_t L_67 = L_66->___value_6;
		L_65->___value_6 = ((int32_t)il2cpp_codegen_subtract(L_67, 1));
		goto IL_016b;
	}

IL_0148:
	{
		// solitaire.bottoms[s1.row].Remove(slot1.name);
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_68 = __this->___solitaire_5;
		List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* L_69 = L_68->___bottoms_11;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_70 = V_0;
		int32_t L_71 = L_70->___row_7;
		int32_t L_72 = L_71;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_73 = (L_69)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_72));
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_74 = __this->___slot1_4;
		String_t* L_75;
		L_75 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_74, NULL);
		bool L_76;
		L_76 = List_1_Remove_mD9E8CFB6777A99046B3C0195F7343FE771A2E99D(L_73, L_75, List_1_Remove_mD9E8CFB6777A99046B3C0195F7343FE771A2E99D_RuntimeMethod_var);
	}

IL_016b:
	{
		// s1.inDeckPile = false; // you cannot add cards to the trips pile so this is always fine
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_77 = V_0;
		L_77->___inDeckPile_9 = (bool)0;
		// s1.row = s2.row;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_78 = V_0;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_79 = V_1;
		int32_t L_80 = L_79->___row_7;
		L_78->___row_7 = L_80;
		// if (s2.top) // moves a card to the top and assigns the top's value and suit
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_81 = V_1;
		bool L_82 = L_81->___top_4;
		if (!L_82)
		{
			goto IL_01d3;
		}
	}
	{
		// solitaire.topPos[s1.row].GetComponent<Selectable>().value = s1.value;
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_83 = __this->___solitaire_5;
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_84 = L_83->___topPos_8;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_85 = V_0;
		int32_t L_86 = L_85->___row_7;
		int32_t L_87 = L_86;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_88 = (L_84)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_87));
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_89;
		L_89 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_88, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_90 = V_0;
		int32_t L_91 = L_90->___value_6;
		L_89->___value_6 = L_91;
		// solitaire.topPos[s1.row].GetComponent<Selectable>().suit = s1.suit;
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_92 = __this->___solitaire_5;
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_93 = L_92->___topPos_8;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_94 = V_0;
		int32_t L_95 = L_94->___row_7;
		int32_t L_96 = L_95;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_97 = (L_93)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_96));
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_98;
		L_98 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_97, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_99 = V_0;
		String_t* L_100 = L_99->___suit_5;
		L_98->___suit_5 = L_100;
		Il2CppCodeGenWriteBarrier((void**)(&L_98->___suit_5), (void*)L_100);
		// s1.top = true;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_101 = V_0;
		L_101->___top_4 = (bool)1;
		goto IL_01da;
	}

IL_01d3:
	{
		// s1.top = false;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_102 = V_0;
		L_102->___top_4 = (bool)0;
	}

IL_01da:
	{
		// slot1 = this.gameObject;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_103;
		L_103 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		__this->___slot1_4 = L_103;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___slot1_4), (void*)L_103);
		// }
		return;
	}
}
// System.Boolean UserInput::Blocked(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UserInput_Blocked_m4830DFF904DB15168334ACD2991538AF1F7ECD18 (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___selected0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Last_TisString_t_m43259774830432D2A85E650A290C92CFB006C36A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral994AF4767E315DC605A2C546002D5A8F201D5D16);
		s_Il2CppMethodInitialized = true;
	}
	Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* V_0 = NULL;
	{
		// Selectable s2 = selected.GetComponent<Selectable>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = ___selected0;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_1;
		L_1 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_0, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		V_0 = L_1;
		// if (s2.inDeckPile == true)
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_2 = V_0;
		bool L_3 = L_2->___inDeckPile_9;
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		// if (s2.name == solitaire.tripsOnDisplay.Last()) // if it is the last trip it is not blocked
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_4 = V_0;
		String_t* L_5;
		L_5 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_4, NULL);
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_6 = __this->___solitaire_5;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_7 = L_6->___tripsOnDisplay_13;
		String_t* L_8;
		L_8 = Enumerable_Last_TisString_t_m43259774830432D2A85E650A290C92CFB006C36A(L_7, Enumerable_Last_TisString_t_m43259774830432D2A85E650A290C92CFB006C36A_RuntimeMethod_var);
		bool L_9;
		L_9 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_5, L_8, NULL);
		if (!L_9)
		{
			goto IL_002e;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_002e:
	{
		// print(s2.name + " is blocked by " + solitaire.tripsOnDisplay.Last());
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_10 = V_0;
		String_t* L_11;
		L_11 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_10, NULL);
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_12 = __this->___solitaire_5;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_13 = L_12->___tripsOnDisplay_13;
		String_t* L_14;
		L_14 = Enumerable_Last_TisString_t_m43259774830432D2A85E650A290C92CFB006C36A(L_13, Enumerable_Last_TisString_t_m43259774830432D2A85E650A290C92CFB006C36A_RuntimeMethod_var);
		String_t* L_15;
		L_15 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_11, _stringLiteral994AF4767E315DC605A2C546002D5A8F201D5D16, L_14, NULL);
		MonoBehaviour_print_mED815C779E369787B3E9646A6DE96FBC2944BF0B(L_15, NULL);
		// return true;
		return (bool)1;
	}

IL_0055:
	{
		// if (s2.name == solitaire.bottoms[s2.row].Last()) // check if it is the bottom card
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_16 = V_0;
		String_t* L_17;
		L_17 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_16, NULL);
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_18 = __this->___solitaire_5;
		List_1U5BU5D_tE510DA387DA867AC92F8274325B178A7DE9A209E* L_19 = L_18->___bottoms_11;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_20 = V_0;
		int32_t L_21 = L_20->___row_7;
		int32_t L_22 = L_21;
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_23 = (L_19)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_22));
		String_t* L_24;
		L_24 = Enumerable_Last_TisString_t_m43259774830432D2A85E650A290C92CFB006C36A(L_23, Enumerable_Last_TisString_t_m43259774830432D2A85E650A290C92CFB006C36A_RuntimeMethod_var);
		bool L_25;
		L_25 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_17, L_24, NULL);
		if (!L_25)
		{
			goto IL_007b;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_007b:
	{
		// return true;
		return (bool)1;
	}
}
// System.Boolean UserInput::DoubleClick()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UserInput_DoubleClick_m5CE515EA8503B13815F9515D4899936F36935CF9 (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCC3B66F53DF5E0F136EE01571B02058391AD2CA1);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (timer < doubleClickTime && clickCount == 2)
		float L_0 = __this->___timer_6;
		float L_1 = __this->___doubleClickTime_7;
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_2 = __this->___clickCount_8;
		if ((!(((uint32_t)L_2) == ((uint32_t)2))))
		{
			goto IL_0023;
		}
	}
	{
		// print("Double Click");
		MonoBehaviour_print_mED815C779E369787B3E9646A6DE96FBC2944BF0B(_stringLiteralCC3B66F53DF5E0F136EE01571B02058391AD2CA1, NULL);
		// return true;
		return (bool)1;
	}

IL_0023:
	{
		// return false;
		return (bool)0;
	}
}
// System.Void UserInput::AutoStack(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInput_AutoStack_mE4DC3420E73DE955AE6987264F0DFD9FD8861B8B (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___selected0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0EBD646B60E1C3FCE0203770591ED3C3D63537DC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3D714DD3E8E77A697EF557E85ED2B014A96328C5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral49A7EA21ECB328D154FA2262BB41626D795F4D90);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEF420ABFDDBDA7B9EE665D85EF62E4A437554003);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* V_1 = NULL;
	String_t* V_2 = NULL;
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* V_3 = NULL;
	{
		// for (int i = 0; i < solitaire.topPos.Length; i++)
		V_0 = 0;
		goto IL_0164;
	}

IL_0007:
	{
		// Selectable stack = solitaire.topPos[i].GetComponent<Selectable>();
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_0 = __this->___solitaire_5;
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_1 = L_0->___topPos_8;
		int32_t L_2 = V_0;
		int32_t L_3 = L_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = (L_1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_3));
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_5;
		L_5 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_4, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		V_1 = L_5;
		// if (selected.GetComponent<Selectable>().value == 1) // if it is an Ace
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6 = ___selected0;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_7;
		L_7 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_6, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		int32_t L_8 = L_7->___value_6;
		if ((!(((uint32_t)L_8) == ((uint32_t)1))))
		{
			goto IL_0058;
		}
	}
	{
		// if (solitaire.topPos[i].GetComponent<Selectable>().value == 0) // and the top position is empty
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_9 = __this->___solitaire_5;
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_10 = L_9->___topPos_8;
		int32_t L_11 = V_0;
		int32_t L_12 = L_11;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13 = (L_10)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_12));
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_14;
		L_14 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_13, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		int32_t L_15 = L_14->___value_6;
		if (L_15)
		{
			goto IL_0160;
		}
	}
	{
		// slot1 = selected;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_16 = ___selected0;
		__this->___slot1_4 = L_16;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___slot1_4), (void*)L_16);
		// Stack(stack.gameObject); // stack the ace up top
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_17 = V_1;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_18;
		L_18 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_17, NULL);
		UserInput_Stack_m072D819EC1B4D6C4A15DDCCF3F121CE90E4695B7(__this, L_18, NULL);
		// break;                  // in the first empty position found
		return;
	}

IL_0058:
	{
		// if ((solitaire.topPos[i].GetComponent<Selectable>().suit == slot1.GetComponent<Selectable>().suit) && (solitaire.topPos[i].GetComponent<Selectable>().value == slot1.GetComponent<Selectable>().value - 1))
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_19 = __this->___solitaire_5;
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_20 = L_19->___topPos_8;
		int32_t L_21 = V_0;
		int32_t L_22 = L_21;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_23 = (L_20)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_22));
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_24;
		L_24 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_23, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		String_t* L_25 = L_24->___suit_5;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_26 = __this->___slot1_4;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_27;
		L_27 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_26, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		String_t* L_28 = L_27->___suit_5;
		bool L_29;
		L_29 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_25, L_28, NULL);
		if (!L_29)
		{
			goto IL_0160;
		}
	}
	{
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_30 = __this->___solitaire_5;
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_31 = L_30->___topPos_8;
		int32_t L_32 = V_0;
		int32_t L_33 = L_32;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_34 = (L_31)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_33));
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_35;
		L_35 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_34, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		int32_t L_36 = L_35->___value_6;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_37 = __this->___slot1_4;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_38;
		L_38 = GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE(L_37, GameObject_GetComponent_TisSelectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745_m62EEFF590B89EE9EFA392DA51D31D4F52EE1F8BE_RuntimeMethod_var);
		int32_t L_39 = L_38->___value_6;
		if ((!(((uint32_t)L_36) == ((uint32_t)((int32_t)il2cpp_codegen_subtract(L_39, 1))))))
		{
			goto IL_0160;
		}
	}
	{
		// if (HasNoChildren(slot1))
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_40 = __this->___slot1_4;
		bool L_41;
		L_41 = UserInput_HasNoChildren_m4A22D7989817199809D0235DD74CB0A1CD5A9FA9(__this, L_40, NULL);
		if (!L_41)
		{
			goto IL_0160;
		}
	}
	{
		// slot1 = selected;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_42 = ___selected0;
		__this->___slot1_4 = L_42;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___slot1_4), (void*)L_42);
		// string lastCardname = stack.suit + stack.value.ToString();
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_43 = V_1;
		String_t* L_44 = L_43->___suit_5;
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_45 = V_1;
		int32_t* L_46 = (&L_45->___value_6);
		String_t* L_47;
		L_47 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_46, NULL);
		String_t* L_48;
		L_48 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(L_44, L_47, NULL);
		V_2 = L_48;
		// if (stack.value == 1)
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_49 = V_1;
		int32_t L_50 = L_49->___value_6;
		if ((!(((uint32_t)L_50) == ((uint32_t)1))))
		{
			goto IL_0100;
		}
	}
	{
		// lastCardname = stack.suit + "A";
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_51 = V_1;
		String_t* L_52 = L_51->___suit_5;
		String_t* L_53;
		L_53 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(L_52, _stringLiteralEF420ABFDDBDA7B9EE665D85EF62E4A437554003, NULL);
		V_2 = L_53;
	}

IL_0100:
	{
		// if (stack.value == 11)
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_54 = V_1;
		int32_t L_55 = L_54->___value_6;
		if ((!(((uint32_t)L_55) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_011b;
		}
	}
	{
		// lastCardname = stack.suit + "J";
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_56 = V_1;
		String_t* L_57 = L_56->___suit_5;
		String_t* L_58;
		L_58 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(L_57, _stringLiteral0EBD646B60E1C3FCE0203770591ED3C3D63537DC, NULL);
		V_2 = L_58;
	}

IL_011b:
	{
		// if (stack.value == 12)
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_59 = V_1;
		int32_t L_60 = L_59->___value_6;
		if ((!(((uint32_t)L_60) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0136;
		}
	}
	{
		// lastCardname = stack.suit + "Q";
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_61 = V_1;
		String_t* L_62 = L_61->___suit_5;
		String_t* L_63;
		L_63 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(L_62, _stringLiteral49A7EA21ECB328D154FA2262BB41626D795F4D90, NULL);
		V_2 = L_63;
	}

IL_0136:
	{
		// if (stack.value == 13)
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_64 = V_1;
		int32_t L_65 = L_64->___value_6;
		if ((!(((uint32_t)L_65) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0151;
		}
	}
	{
		// lastCardname = stack.suit + "K";
		Selectable_t25B15B7C21E3C9F6CC66F175572AF7226F108745* L_66 = V_1;
		String_t* L_67 = L_66->___suit_5;
		String_t* L_68;
		L_68 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(L_67, _stringLiteral3D714DD3E8E77A697EF557E85ED2B014A96328C5, NULL);
		V_2 = L_68;
	}

IL_0151:
	{
		// GameObject lastCard = GameObject.Find(lastCardname);
		String_t* L_69 = V_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_70;
		L_70 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(L_69, NULL);
		V_3 = L_70;
		// Stack(lastCard);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_71 = V_3;
		UserInput_Stack_m072D819EC1B4D6C4A15DDCCF3F121CE90E4695B7(__this, L_71, NULL);
		// break;
		return;
	}

IL_0160:
	{
		// for (int i = 0; i < solitaire.topPos.Length; i++)
		int32_t L_72 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_72, 1));
	}

IL_0164:
	{
		// for (int i = 0; i < solitaire.topPos.Length; i++)
		int32_t L_73 = V_0;
		Solitaire_t43D61047DE323D7D7559F84668F499D389CC1240* L_74 = __this->___solitaire_5;
		GameObjectU5BU5D_tFF67550DFCE87096D7A3734EA15B75896B2722CF* L_75 = L_74->___topPos_8;
		if ((((int32_t)L_73) < ((int32_t)((int32_t)(((RuntimeArray*)L_75)->max_length)))))
		{
			goto IL_0007;
		}
	}
	{
		// }
		return;
	}
}
// System.Boolean UserInput::HasNoChildren(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UserInput_HasNoChildren_m4A22D7989817199809D0235DD74CB0A1CD5A9FA9 (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___card0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	{
		// int i = 0;
		V_0 = 0;
		// foreach (Transform child in card.transform)
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = ___card0;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_1;
		L_1 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_0, NULL);
		RuntimeObject* L_2;
		L_2 = Transform_GetEnumerator_mA7E1C882ACA0C33E284711CD09971DEA3FFEF404(L_1, NULL);
		V_1 = L_2;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_002a:
			{// begin finally (depth: 1)
				{
					RuntimeObject* L_3 = V_1;
					V_2 = ((RuntimeObject*)IsInst((RuntimeObject*)L_3, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var));
					RuntimeObject* L_4 = V_2;
					if (!L_4)
					{
						goto IL_003a;
					}
				}
				{
					RuntimeObject* L_5 = V_2;
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_5);
				}

IL_003a:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0020_1;
			}

IL_0010_1:
			{
				// foreach (Transform child in card.transform)
				RuntimeObject* L_6 = V_1;
				RuntimeObject* L_7;
				L_7 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_6);
				// i++;
				int32_t L_8 = V_0;
				V_0 = ((int32_t)il2cpp_codegen_add(L_8, 1));
			}

IL_0020_1:
			{
				// foreach (Transform child in card.transform)
				RuntimeObject* L_9 = V_1;
				bool L_10;
				L_10 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_9);
				if (L_10)
				{
					goto IL_0010_1;
				}
			}
			{
				goto IL_003b;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_003b:
	{
		// if (i == 0)
		int32_t L_11 = V_0;
		if (L_11)
		{
			goto IL_0040;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_0040:
	{
		// return false;
		return (bool)0;
	}
}
// System.Void UserInput::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInput__ctor_mEFD22D7842A3490C3456F3174DABEFA0BFB52669 (UserInput_t2EA1DBEAE91DF5705BE14F933C71095DCD67D1E7* __this, const RuntimeMethod* method) 
{
	{
		// private float doubleClickTime = 0.3f;
		__this->___doubleClickTime_7 = (0.300000012f);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____stringLength_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_2 = L_0;
		float L_1 = ___y1;
		__this->___y_3 = L_1;
		float L_2 = ___z2;
		__this->___z_4 = L_2;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_get_identity_mB9CAEEB21BC81352CBF32DB9664BFC06FA7EA27B_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = ((Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields*)il2cpp_codegen_static_fields_for(Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_il2cpp_TypeInfo_var))->___identityQuaternion_4;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (1.0f), (0.921568632f), (0.0156862754f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_white_m28BB6E19F27D4EE6858D3021A44F62BC74E20C43_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_op_Implicit_m8F73B300CB4E6F9B4EB5FB6130363D76CEAA230B_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___v0, const RuntimeMethod* method) 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___v0;
		float L_1 = L_0.___x_2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2 = ___v0;
		float L_3 = L_2.___y_3;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_4), L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 Vector2_get_zero_m009B92B5D35AB02BD1610C2E1ACCE7C9CF964A6E_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ((Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_il2cpp_TypeInfo_var))->___zeroVector_2;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		if (!true)
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->____size_2;
		V_0 = L_1;
		__this->____size_2 = 0;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_3 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)__this->____items_1;
		int32_t L_4 = V_0;
		Array_Clear_m48B57EC27CADC3463CA98A33373D557DA587FF1B((RuntimeArray*)L_3, 0, L_4, NULL);
		return;
	}

IL_0035:
	{
		__this->____size_2 = 0;
	}

IL_003c:
	{
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->____current_3;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) 
{
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_1 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = V_0;
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_6 = V_0;
		int32_t L_7 = V_1;
		RuntimeObject* L_8 = ___item0;
		(L_6)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_7), (RuntimeObject*)L_8);
		return;
	}

IL_0034:
	{
		RuntimeObject* L_9 = ___item0;
		((  void (*) (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D*, RuntimeObject*, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = (int32_t)__this->____size_2;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) 
{
	{
		float L_0 = ___r0;
		__this->___r_0 = L_0;
		float L_1 = ___g1;
		__this->___g_1 = L_1;
		float L_2 = ___b2;
		__this->___b_2 = L_2;
		float L_3 = ___a3;
		__this->___a_3 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___x0, float ___y1, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_0 = L_0;
		float L_1 = ___y1;
		__this->___y_1 = L_1;
		return;
	}
}
