﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void ScoreKeeper::Start()
extern void ScoreKeeper_Start_m111876C645A6D668BC752821007BE6877002C6A3 (void);
// 0x00000002 System.Void ScoreKeeper::Update()
extern void ScoreKeeper_Update_m31E4B2F16670B50AD481128A788BF2ED3B607179 (void);
// 0x00000003 System.Boolean ScoreKeeper::HasWon()
extern void ScoreKeeper_HasWon_mCA808C163BB4AFE1B799C0C9A14FDD8CEF69C450 (void);
// 0x00000004 System.Void ScoreKeeper::Win()
extern void ScoreKeeper_Win_mA7910B2F38648B795B67DE595622EFB542DCE565 (void);
// 0x00000005 System.Void ScoreKeeper::.ctor()
extern void ScoreKeeper__ctor_m2ACACD5BEC5B2FB90A3D014C60E3726F789EE357 (void);
// 0x00000006 System.Void Selectable::Start()
extern void Selectable_Start_mBA7C139FC1AC9F6F234E222761AB29EAA295B957 (void);
// 0x00000007 System.Void Selectable::Update()
extern void Selectable_Update_m24375E446EFA2E30F944F428F81DEA5B7A2D457C (void);
// 0x00000008 System.Void Selectable::.ctor()
extern void Selectable__ctor_mD3D03CF7FC80D1BEBFF53ED8BB01A4A1E16257D1 (void);
// 0x00000009 System.Void Solitaire::Start()
extern void Solitaire_Start_m75E2A2392463F6D4123EA09D14AD4313140ED0ED (void);
// 0x0000000A System.Void Solitaire::Update()
extern void Solitaire_Update_m41FC684A6F34B3F92DDD2F003ACCA59B1B8235EE (void);
// 0x0000000B System.Void Solitaire::PlayCards()
extern void Solitaire_PlayCards_m72D3A39228F204F5FDCAFA486A64D92459F2AFB0 (void);
// 0x0000000C System.Collections.Generic.List`1<System.String> Solitaire::GenerateDeck()
extern void Solitaire_GenerateDeck_mE430BEF13A823C578E35C2137BFA3A660BE22446 (void);
// 0x0000000D System.Void Solitaire::Shuffle(System.Collections.Generic.List`1<T>)
// 0x0000000E System.Collections.IEnumerator Solitaire::SolitaireDeal()
extern void Solitaire_SolitaireDeal_mF910B118C91BF7DE326EA9489F36A28F003FEF61 (void);
// 0x0000000F System.Void Solitaire::SolitaireSort()
extern void Solitaire_SolitaireSort_m6403D2229F335EE10D5629B2B319726858989CE5 (void);
// 0x00000010 System.Void Solitaire::SortDeckIntoTrips()
extern void Solitaire_SortDeckIntoTrips_m6A43E9681644C41D6B7743B9B0B45A5066490ADB (void);
// 0x00000011 System.Void Solitaire::DealFromDeck()
extern void Solitaire_DealFromDeck_m34AC6775F731EFFCF36FED448B595069B90709AB (void);
// 0x00000012 System.Void Solitaire::RestackTopDeck()
extern void Solitaire_RestackTopDeck_m70300CC871C5AC743DB5C8A7234995430030C466 (void);
// 0x00000013 System.Void Solitaire::.ctor()
extern void Solitaire__ctor_m0129122DD857610D6D8B174913E3A5822FB0CFDA (void);
// 0x00000014 System.Void Solitaire::.cctor()
extern void Solitaire__cctor_mE77011A998AE2030696238949035383AA594B6E8 (void);
// 0x00000015 System.Void Solitaire/<SolitaireDeal>d__28::.ctor(System.Int32)
extern void U3CSolitaireDealU3Ed__28__ctor_m4853FC99373F6E44BEF3F1AEF9AB8AF90B6829B9 (void);
// 0x00000016 System.Void Solitaire/<SolitaireDeal>d__28::System.IDisposable.Dispose()
extern void U3CSolitaireDealU3Ed__28_System_IDisposable_Dispose_m35614D5BEBE733B6F300892713EADCEA91F7D394 (void);
// 0x00000017 System.Boolean Solitaire/<SolitaireDeal>d__28::MoveNext()
extern void U3CSolitaireDealU3Ed__28_MoveNext_mC28F87192A8F4206F1710D4320495772821D24B8 (void);
// 0x00000018 System.Void Solitaire/<SolitaireDeal>d__28::<>m__Finally1()
extern void U3CSolitaireDealU3Ed__28_U3CU3Em__Finally1_mE37C5D2D2EEBDE3FF25159019FB5691FE02237AA (void);
// 0x00000019 System.Object Solitaire/<SolitaireDeal>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSolitaireDealU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCC1D7ECECACC853665B21D55A7F363EC1BE2C2E0 (void);
// 0x0000001A System.Void Solitaire/<SolitaireDeal>d__28::System.Collections.IEnumerator.Reset()
extern void U3CSolitaireDealU3Ed__28_System_Collections_IEnumerator_Reset_m2B23C74A6DCDF1A9C0C25A801E8D3D2C4FC090BC (void);
// 0x0000001B System.Object Solitaire/<SolitaireDeal>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CSolitaireDealU3Ed__28_System_Collections_IEnumerator_get_Current_m44AB5AB5EAF8AF55792221CF6BF19E6F6271C729 (void);
// 0x0000001C System.Void UIButtons::Start()
extern void UIButtons_Start_mCE48599A11D55FCCFAFD47F3805EEC21ECA2F951 (void);
// 0x0000001D System.Void UIButtons::Update()
extern void UIButtons_Update_m8F0E7C758C89C37ED0B4B11A937C125BBDFA66A3 (void);
// 0x0000001E System.Void UIButtons::PlayAgain()
extern void UIButtons_PlayAgain_m2F4D930000DBF639DE2CF6D360FB171E53E73CC4 (void);
// 0x0000001F System.Void UIButtons::ResetScene()
extern void UIButtons_ResetScene_m12CD70C4CF320019CA5D036840D9E0A2F3CCC433 (void);
// 0x00000020 System.Void UIButtons::ClearTopValues()
extern void UIButtons_ClearTopValues_m22554521111B98CD12C46FDF3EBFB171DF3A41A6 (void);
// 0x00000021 System.Void UIButtons::.ctor()
extern void UIButtons__ctor_mF3BA7757F4EE0871787B6C68BDDC2D3DCF905FF7 (void);
// 0x00000022 System.Void UpdateSprite::Start()
extern void UpdateSprite_Start_m266DCAA8C866FA5D638AC7183B894D203C3ED802 (void);
// 0x00000023 System.Void UpdateSprite::Update()
extern void UpdateSprite_Update_m75EAD54828ED22075EA0489B30B8437863C6B43C (void);
// 0x00000024 System.Void UpdateSprite::.ctor()
extern void UpdateSprite__ctor_mD147FFD98ACF28F2A52DDAF980BE7B6F975DD908 (void);
// 0x00000025 System.Void UserInput::Start()
extern void UserInput_Start_mC73B8E0EE61359EC33603FD127DE524A70A9356F (void);
// 0x00000026 System.Void UserInput::Update()
extern void UserInput_Update_m2BA028D4EC83095DFA9C6BF8E30434294465B6AF (void);
// 0x00000027 System.Void UserInput::GetMouseClick()
extern void UserInput_GetMouseClick_m25267C57BDB8466106B107405074860BAD41E21C (void);
// 0x00000028 System.Void UserInput::Deck()
extern void UserInput_Deck_m81436BDC023279A2DF8FC3C7B07C18DEB6BAA7A4 (void);
// 0x00000029 System.Void UserInput::Card(UnityEngine.GameObject)
extern void UserInput_Card_m61BB20A7E54456DBE7881A622A3FCE4E7A1C001F (void);
// 0x0000002A System.Void UserInput::Top(UnityEngine.GameObject)
extern void UserInput_Top_m6D128BE13556EC650D464A52115774A60646862E (void);
// 0x0000002B System.Void UserInput::Bottom(UnityEngine.GameObject)
extern void UserInput_Bottom_m52CFBCBDEC44D99AB54CAB9C14B986085AF41C69 (void);
// 0x0000002C System.Boolean UserInput::Stackable(UnityEngine.GameObject)
extern void UserInput_Stackable_m8F68A76920F4753B849A7FE5C7655711C0BD4BD2 (void);
// 0x0000002D System.Void UserInput::Stack(UnityEngine.GameObject)
extern void UserInput_Stack_m072D819EC1B4D6C4A15DDCCF3F121CE90E4695B7 (void);
// 0x0000002E System.Boolean UserInput::Blocked(UnityEngine.GameObject)
extern void UserInput_Blocked_m4830DFF904DB15168334ACD2991538AF1F7ECD18 (void);
// 0x0000002F System.Boolean UserInput::DoubleClick()
extern void UserInput_DoubleClick_m5CE515EA8503B13815F9515D4899936F36935CF9 (void);
// 0x00000030 System.Void UserInput::AutoStack(UnityEngine.GameObject)
extern void UserInput_AutoStack_mE4DC3420E73DE955AE6987264F0DFD9FD8861B8B (void);
// 0x00000031 System.Boolean UserInput::HasNoChildren(UnityEngine.GameObject)
extern void UserInput_HasNoChildren_m4A22D7989817199809D0235DD74CB0A1CD5A9FA9 (void);
// 0x00000032 System.Void UserInput::.ctor()
extern void UserInput__ctor_mEFD22D7842A3490C3456F3174DABEFA0BFB52669 (void);
static Il2CppMethodPointer s_methodPointers[50] = 
{
	ScoreKeeper_Start_m111876C645A6D668BC752821007BE6877002C6A3,
	ScoreKeeper_Update_m31E4B2F16670B50AD481128A788BF2ED3B607179,
	ScoreKeeper_HasWon_mCA808C163BB4AFE1B799C0C9A14FDD8CEF69C450,
	ScoreKeeper_Win_mA7910B2F38648B795B67DE595622EFB542DCE565,
	ScoreKeeper__ctor_m2ACACD5BEC5B2FB90A3D014C60E3726F789EE357,
	Selectable_Start_mBA7C139FC1AC9F6F234E222761AB29EAA295B957,
	Selectable_Update_m24375E446EFA2E30F944F428F81DEA5B7A2D457C,
	Selectable__ctor_mD3D03CF7FC80D1BEBFF53ED8BB01A4A1E16257D1,
	Solitaire_Start_m75E2A2392463F6D4123EA09D14AD4313140ED0ED,
	Solitaire_Update_m41FC684A6F34B3F92DDD2F003ACCA59B1B8235EE,
	Solitaire_PlayCards_m72D3A39228F204F5FDCAFA486A64D92459F2AFB0,
	Solitaire_GenerateDeck_mE430BEF13A823C578E35C2137BFA3A660BE22446,
	NULL,
	Solitaire_SolitaireDeal_mF910B118C91BF7DE326EA9489F36A28F003FEF61,
	Solitaire_SolitaireSort_m6403D2229F335EE10D5629B2B319726858989CE5,
	Solitaire_SortDeckIntoTrips_m6A43E9681644C41D6B7743B9B0B45A5066490ADB,
	Solitaire_DealFromDeck_m34AC6775F731EFFCF36FED448B595069B90709AB,
	Solitaire_RestackTopDeck_m70300CC871C5AC743DB5C8A7234995430030C466,
	Solitaire__ctor_m0129122DD857610D6D8B174913E3A5822FB0CFDA,
	Solitaire__cctor_mE77011A998AE2030696238949035383AA594B6E8,
	U3CSolitaireDealU3Ed__28__ctor_m4853FC99373F6E44BEF3F1AEF9AB8AF90B6829B9,
	U3CSolitaireDealU3Ed__28_System_IDisposable_Dispose_m35614D5BEBE733B6F300892713EADCEA91F7D394,
	U3CSolitaireDealU3Ed__28_MoveNext_mC28F87192A8F4206F1710D4320495772821D24B8,
	U3CSolitaireDealU3Ed__28_U3CU3Em__Finally1_mE37C5D2D2EEBDE3FF25159019FB5691FE02237AA,
	U3CSolitaireDealU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCC1D7ECECACC853665B21D55A7F363EC1BE2C2E0,
	U3CSolitaireDealU3Ed__28_System_Collections_IEnumerator_Reset_m2B23C74A6DCDF1A9C0C25A801E8D3D2C4FC090BC,
	U3CSolitaireDealU3Ed__28_System_Collections_IEnumerator_get_Current_m44AB5AB5EAF8AF55792221CF6BF19E6F6271C729,
	UIButtons_Start_mCE48599A11D55FCCFAFD47F3805EEC21ECA2F951,
	UIButtons_Update_m8F0E7C758C89C37ED0B4B11A937C125BBDFA66A3,
	UIButtons_PlayAgain_m2F4D930000DBF639DE2CF6D360FB171E53E73CC4,
	UIButtons_ResetScene_m12CD70C4CF320019CA5D036840D9E0A2F3CCC433,
	UIButtons_ClearTopValues_m22554521111B98CD12C46FDF3EBFB171DF3A41A6,
	UIButtons__ctor_mF3BA7757F4EE0871787B6C68BDDC2D3DCF905FF7,
	UpdateSprite_Start_m266DCAA8C866FA5D638AC7183B894D203C3ED802,
	UpdateSprite_Update_m75EAD54828ED22075EA0489B30B8437863C6B43C,
	UpdateSprite__ctor_mD147FFD98ACF28F2A52DDAF980BE7B6F975DD908,
	UserInput_Start_mC73B8E0EE61359EC33603FD127DE524A70A9356F,
	UserInput_Update_m2BA028D4EC83095DFA9C6BF8E30434294465B6AF,
	UserInput_GetMouseClick_m25267C57BDB8466106B107405074860BAD41E21C,
	UserInput_Deck_m81436BDC023279A2DF8FC3C7B07C18DEB6BAA7A4,
	UserInput_Card_m61BB20A7E54456DBE7881A622A3FCE4E7A1C001F,
	UserInput_Top_m6D128BE13556EC650D464A52115774A60646862E,
	UserInput_Bottom_m52CFBCBDEC44D99AB54CAB9C14B986085AF41C69,
	UserInput_Stackable_m8F68A76920F4753B849A7FE5C7655711C0BD4BD2,
	UserInput_Stack_m072D819EC1B4D6C4A15DDCCF3F121CE90E4695B7,
	UserInput_Blocked_m4830DFF904DB15168334ACD2991538AF1F7ECD18,
	UserInput_DoubleClick_m5CE515EA8503B13815F9515D4899936F36935CF9,
	UserInput_AutoStack_mE4DC3420E73DE955AE6987264F0DFD9FD8861B8B,
	UserInput_HasNoChildren_m4A22D7989817199809D0235DD74CB0A1CD5A9FA9,
	UserInput__ctor_mEFD22D7842A3490C3456F3174DABEFA0BFB52669,
};
static const int32_t s_InvokerIndices[50] = 
{
	3173,
	3173,
	3037,
	3173,
	3173,
	3173,
	3173,
	3173,
	3173,
	3173,
	3173,
	4809,
	0,
	3092,
	3173,
	3173,
	3173,
	3173,
	3173,
	4833,
	2578,
	3173,
	3037,
	3173,
	3092,
	3173,
	3092,
	3173,
	3173,
	3173,
	3173,
	3173,
	3173,
	3173,
	3173,
	3173,
	3173,
	3173,
	3173,
	3173,
	2594,
	2594,
	2594,
	1854,
	2594,
	1854,
	3037,
	2594,
	1854,
	3173,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x0600000D, { 0, 4 } },
};
extern const uint32_t g_rgctx_List_1_t6785015FFA932BBE77C0464484DD0D0B3183177C;
extern const uint32_t g_rgctx_List_1_get_Count_m03278BEDECC45AF44D0F4C02D2511C91BBD33EAD;
extern const uint32_t g_rgctx_List_1_get_Item_m89A0C247A0DB5695CB108DC32D60CAD7241E01B4;
extern const uint32_t g_rgctx_List_1_set_Item_m6E21447A523DD24C1F92AEE9FE79E793CA636DDA;
static const Il2CppRGCTXDefinition s_rgctxValues[4] = 
{
	{ (Il2CppRGCTXDataType)2, (const void *)&g_rgctx_List_1_t6785015FFA932BBE77C0464484DD0D0B3183177C },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Count_m03278BEDECC45AF44D0F4C02D2511C91BBD33EAD },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_get_Item_m89A0C247A0DB5695CB108DC32D60CAD7241E01B4 },
	{ (Il2CppRGCTXDataType)3, (const void *)&g_rgctx_List_1_set_Item_m6E21447A523DD24C1F92AEE9FE79E793CA636DDA },
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	50,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	4,
	s_rgctxValues,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
